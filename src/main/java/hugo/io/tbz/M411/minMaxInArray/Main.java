package hugo.io.tbz.M411.minMaxInArray;

import hugo.io.other.cosoleUtility.myconsole.MyConsole;

public class Main {
    private static Integer MIN = Integer.MAX_VALUE;
    private static Integer MAX = 0;

    public static void main(String[] Args) {
        int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 11};
        for (int i : array) {
            if (i < MIN) {
                MIN = i;
            }
            if (i > MAX) {
                MAX = i;
            }
        }
        MyConsole console = new MyConsole();
        for (int i : array) {
            console.print(i + ";");
        }
        console.printLine();
        console.printLine("max value = " + MAX);
        console.printLine("min value = " + MIN);
    }
}