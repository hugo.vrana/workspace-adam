package hugo.io.tbz.M411.showReversedText;

import hugo.io.other.cosoleUtility.myconsole.MyConsole;

public class Main {
    public static void main(String[] argv) {
        MyConsole console = new MyConsole();
        console.printLine("Write text");
        String str = console.readLineString();
        char[] arr = str.toCharArray();
        String reversedStr = "";
        for (Character c : arr) {
            reversedStr = c + reversedStr;
        }
        console.printLine("String: " + str);
        console.printLine("Reversed string: " + reversedStr);
    }
}
