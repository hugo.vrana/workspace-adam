package hugo.io.tbz.M411.linkedListTestProgram;

import hugo.io.other.cosoleUtility.myconsole.MyConsole;
import hugo.io.tbz.M411.linkedList.LinkedList;
import hugo.io.tbz.M411.linkedList.Node;

public class Main {
    public static void main(String[] args) {
        MyConsole console = new MyConsole();
        console.printLine("Please enter any string.");
        String str = console.readLineString();
        char[] arr = str.toCharArray();
        LinkedList ll = new LinkedList();
        for (Character c : arr) {
            ll.add(new Node(c.toString()));
        }
        ll.showElements();
    }
}
