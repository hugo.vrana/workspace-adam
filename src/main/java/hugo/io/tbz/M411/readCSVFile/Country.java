package hugo.io.tbz.M411.readCSVFile;

public class Country {
    private String isoAbservation;
    private String name;
    private String capital;

    public String getIsoAbservation() {
        return isoAbservation;
    }

    public void setIsoAbservation(String isoAbservation) {
        this.isoAbservation = isoAbservation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

}
