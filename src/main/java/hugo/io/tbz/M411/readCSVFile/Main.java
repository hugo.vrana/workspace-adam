package hugo.io.tbz.M411.readCSVFile;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class Main {
    public static void main(String[] args) {
        String csvFile = "/home/hugo/workspaces/workspace-adam/src/Main/java/hugo/io/readCSVFile/resuorces/countries_simplified.xls";
        ArrayList<String[]> countries = readFile(csvFile);
        HashMap<String, Country> countryHashMap = toMap(countries);
        for (Map.Entry c : countryHashMap.entrySet()) {
            System.out.println(c.getKey() + " " + c.getValue());
        }
    }

    private static ArrayList readFile(String path) {
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
        ArrayList<String[]> lines = new ArrayList<>();

        try {
            br = new BufferedReader(new FileReader(path));
            while ((line = br.readLine()) != null) {

                // use comma as separator
                if (!line.equals("3-letter ISO abbreviation,name,capital;")) {
                    String[] country = line.split(cvsSplitBy);
                    lines.add(country);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return lines;
        }
    }

    private static HashMap<String, Country> toMap(ArrayList<String[]> rawCountries) {
        HashMap<String, Country> countries = new HashMap();
        for (String[] countryString : rawCountries) {
            Country country = new Country();
            country.setIsoAbservation(countryString[0]);
            country.setName(countryString[1]);
            country.setCapital(countryString[2]);
            countries.put(country.getIsoAbservation(), country);
        }
        return countries;
    }


}
