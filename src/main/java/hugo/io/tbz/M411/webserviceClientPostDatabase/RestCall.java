package hugo.io.tbz.M411.webserviceClientPostDatabase;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.Charset;

public class RestCall {

    // Requests JSON from any url and
    //<param type = String name = urlString> url, from whitch the JSON should be requested</param>
    // returns JSON as a String
    public String getBody(String urlString) {
        String json = "";
        try {
            URL url = new URL(urlString); // URL from JSON API
            InputStream is = url.openStream();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
                StringBuilder sb = new StringBuilder();
                int cp;
                while ((cp = rd.read()) != -1) {
                    sb.append((char) cp);
                }
                is.close();
                // any data that came are now in string
                json = sb.toString();
            } catch (Exception ex) {
                System.out.println("JSON reading error.");
            }
        } catch (Exception ex) {
            System.out.println("Connection could not be estamblished.");
        } finally {
            return json;
        }
    }
}
