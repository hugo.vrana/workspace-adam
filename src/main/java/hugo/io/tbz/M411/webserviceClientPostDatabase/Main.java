package hugo.io.tbz.M411.webserviceClientPostDatabase;

import hugo.io.other.cosoleUtility.myconsole.MyConsole;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        /*
        RestCall restCall = new RestCall();
        String json = restCall.getBody("https://jsonplaceholder.typicode.com/posts");
        String[] jsonArray = json.split("},");

        Map<Integer, Post> postMap = new HashMap<>();
        for (String s : jsonArray) {
            Post post = new Post();
            post = post.fromString(s + '}');
            if (post.getId() != null) {
                postMap.put(post.getId(), post);
            }
        }

        MyConsole console = new MyConsole();
        ArrayList<String> mainMenu = new ArrayList<>();
        mainMenu.add("Post List");
        mainMenu.add("Print one specific post");
        mainMenu.add("Search for any post");
        mainMenu.add("Exit");

        while (true) {
            console.printMenu(mainMenu);
            Integer chosen = console.readLineInt();
            switch (chosen) {
                case 1:
                    printPostList(postMap);
                    break;
                case 2:
                    printPost(postMap);
                    break;
                case 3:
                    search(postMap);
                    break;
                case 4:
                    System.exit(0);
                    break;
                default:
                    console.printLine("Wrong value");
                    break;
            }
        }*/
    }

    // Searches all Posts by any Attribute they have
    //<param type = Map<Integer, Post> name = postMap> map with all posts</param>
    private static void search(Map<Integer, Post> postMap) {
        /*
        MyConsole console = new MyConsole();
        Map<Integer, Post> searchResults = new HashMap<>();
        console.printLine();
        console.printLine("Now you can search for values");
        ArrayList<String> menu = new ArrayList<>();
        menu.add("UserId");
        menu.add("Id");
        menu.add("Title");
        menu.add("Body");
        menu.add("Exit the application");
        console.printMenu(menu);
        Integer category = console.readLineInt();
        switch (category) {
            case 1:
                console.printLine("Insert the UserId value");
                Integer searchedUserId = console.readLineInt();
                searchResults = postMap
                        .entrySet()
                        .stream()
                        .filter(p -> p.getValue().getUserId().equals(searchedUserId))
                        .collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue()));
                break;
            case 2:
                console.printLine("Insert the Id value");
                Integer searchedId = console.readLineInt();
                searchResults = postMap
                        .entrySet()
                        .stream()
                        .filter(p -> p.getValue().getId().equals(searchedId))
                        .collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue()));
                break;
            case 3:
                console.printLine("Insert the Title value");
                String searchedTitle = console.readLineString();
                searchResults = postMap
                        .entrySet()
                        .stream()
                        .filter(p -> p.getValue().getTitle().contains(searchedTitle))
                        .collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue()));
                break;
            case 4:
                console.printLine("Insert the Body value");
                String searchedBody = console.readLineString();
                searchResults = postMap
                        .entrySet()
                        .stream()
                        .filter(p -> p.getValue().getBody().contains(searchedBody))
                        .collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue()));
                break;
            case 5:
                System.exit(0);
                break;
            default:
                console.printLine("You entered wrong value");
                return;
        }
        if (!searchResults.isEmpty()) {
            for (Post p : searchResults.values()) {
                console.printLine();
                console.printLine("UserId = " + p.getUserId());
                console.printLine("Id = " + p.getId());
                console.printLine("Title = " + p.getTitle());
                if (p.getId() == 100) {
                    console.printLine("Body = " + p.getBody().replace(']', ' '));
                } else {
                    console.printLine("Body = " + p.getBody());
                }
            }
            console.printLine();
        } else {
            console.printLine("There are no search reults.");
            console.printLine("");
        }
        */
    }

    // prints every post
    //<param type = Map<Integer, Post> name = postMap> all posts</param>
    // returns JSON as a String
    private static void printPostList(Map<Integer, Post> postMap) {
       /*
        MyConsole console = new MyConsole();
        for (Post p : postMap.values()) {
            if (p.getId() == 100) {
                p.setBody(p.getBody().replace(']', ' '));
            }
            console.printLine("Id = " + p.getId() + " ,Title = " + p.getBody());
        }
        */
    }

    ///prints single post by inserted id
    //<param type = Map<Integer, Post> name = postMap> all posts</param>
    private static void printPost(Map<Integer, Post> postMap) {
      /*
        MyConsole console = new MyConsole();
        console.printLine("Insert the Id value");
        Integer searchedId = console.readLineInt();

        Map<Integer, Post> searchResults = postMap
                .entrySet()
                .stream()
                .filter(p -> p.getValue().getId().equals(searchedId))
                .collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue()));

        Post requested = searchResults.entrySet().iterator().next().getValue();
        console.printLine("UserId = " + requested.getUserId());
        console.printLine("Id = " + requested.getId());
        console.printLine("Title = " + requested.getTitle());
        console.printLine("Body = " + requested.getBody());
        console.printLine();
        */
    }
}