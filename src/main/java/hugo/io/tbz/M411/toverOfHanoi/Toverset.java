package hugo.io.tbz.M411.toverOfHanoi;

import hugo.io.other.cosoleUtility.myconsole.MyConsole;

import java.util.ArrayList;

public class Toverset {
    private ArrayList<Integer> firstTover = new ArrayList<Integer>();
    private ArrayList<Integer> secondTover = new ArrayList<Integer>();
    private ArrayList<Integer> thirdTover = new ArrayList<Integer>();
    private int diskCount = 3;

    public Toverset() {
        // intitialize first tover with 5 disks
        firstTover.add(5);
        firstTover.add(4);
        firstTover.add(3);
        firstTover.add(2);
        firstTover.add(1);
    }

    public ArrayList<Integer> getFirstTover() {
        return firstTover;
    }

    public ArrayList<Integer> getSecondTover() {
        return secondTover;
    }

    public ArrayList<Integer> getThirdTover() {
        return thirdTover;
    }

    public int getDiskCount() {
        return diskCount;
    }

    // movements
    public void moveFirstToSecond() {
        Integer firstTop = firstTover.get(firstTover.size() - 1);
        firstTover.remove(firstTop);
        secondTover.add(firstTop);
    }

    public void moveFirstToThird() {
        Integer firstTop = firstTover.get(firstTover.size() - 1);
        firstTover.remove(firstTop);
        thirdTover.add(firstTop);
    }

    public void moveSecondToFirst() {
        Integer secondTop = secondTover.get(secondTover.size() - 1);
        secondTover.remove(secondTop);
        firstTover.add(secondTop);
    }

    public void moveSecondToThird() {
        Integer secondTop = secondTover.get(secondTover.size() - 1);
        secondTover.remove(secondTop);
        thirdTover.add(secondTop);
    }

    public void moveThirdToFirts() {
        Integer thirdTop = thirdTover.get(thirdTover.size() - 1);
        thirdTover.remove(thirdTop);
        firstTover.add(thirdTop);
    }

    public void moveThirdToSecond() {
        Integer thirdTop = thirdTover.get(thirdTover.size() - 1);
        thirdTover.remove(thirdTop);
        secondTover.add(thirdTop);
    }

    public void printTovers() {
        MyConsole console = new MyConsole();
        console.print("First tover: ");
        for (Integer i : firstTover) {
            console.print(i + " ");
        }
        console.printLine();
        console.print("Second tover: ");
        for (Integer j : secondTover) {
            console.print(j + " ");
        }
        console.printLine();
        console.print("Third tover: ");
        for (Integer k : thirdTover) {
            console.print(k + " ");
        }
    }
}
