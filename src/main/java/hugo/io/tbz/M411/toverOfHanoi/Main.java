package hugo.io.tbz.M411.toverOfHanoi;

public class Main {
    public static void main(String[] args) {
        Toverset toverset = new Toverset();

        toverset.moveFirstToThird();
        toverset.moveFirstToSecond();
        toverset.moveThirdToSecond();
        toverset.moveFirstToThird();
        toverset.moveSecondToFirst();
        toverset.moveSecondToThird();
        toverset.moveFirstToThird();
        toverset.moveFirstToSecond();
        toverset.moveThirdToFirts();
        toverset.moveThirdToSecond();
        toverset.moveFirstToThird();
        toverset.moveSecondToFirst();
        toverset.moveThirdToFirts();
        toverset.moveThirdToSecond();
        toverset.moveFirstToThird();
        toverset.moveFirstToSecond();
        toverset.moveThirdToSecond();
        toverset.moveFirstToThird();
        // first number on third
        toverset.moveSecondToFirst();
        toverset.moveSecondToThird();
        toverset.moveFirstToThird();
        toverset.moveSecondToFirst();
        toverset.moveThirdToSecond();
        toverset.moveThirdToFirts();
        toverset.moveSecondToFirst();
        toverset.moveSecondToThird();
        // second number on third
        toverset.moveFirstToThird();
        toverset.moveFirstToSecond();
        toverset.moveThirdToSecond();
        toverset.moveFirstToThird();
        // third number on third
        toverset.moveSecondToFirst();
        toverset.moveSecondToThird();
        // second number on third
        toverset.moveFirstToThird();

        toverset.printTovers();
    }
}
