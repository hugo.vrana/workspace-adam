package hugo.io.tbz.M411.sorting.quicksort;

import hugo.io.other.cosoleUtility.myconsole.MyConsole;

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        int[] numbers = new int[20];
        Random random = new Random();
        // fill array with random numebers
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = random.nextInt();
        }

        Long timeStart = System.currentTimeMillis();
        Quicksort sorter = new Quicksort();
        sorter.sort(numbers);
        Long timeEnd = System.currentTimeMillis();
        Long time = timeEnd - timeStart;

        MyConsole console = new MyConsole();
        console.printLine("Sorted array :");
        for (int i : numbers) {
            console.printLine(i);
        }
        console.printLine("Sorted 20 Elements in " + time + " miliseconds");
    }
}