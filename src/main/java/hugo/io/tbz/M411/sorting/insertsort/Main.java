package hugo.io.tbz.M411.sorting.insertsort;

import hugo.io.other.cosoleUtility.myconsole.MyConsole;

import java.util.Arrays;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        int[] numbers = new int[20];
        Random random = new Random();
        // fill array with random numebers
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = random.nextInt();
        }

        int steps = 0;
        while (!checkSorting(numbers)) {
            int indexUnrevealdNumber = 0;
            while (indexUnrevealdNumber < numbers.length) {
                for (int x = 0; x < indexUnrevealdNumber; x++) {
                    int n1 = numbers[x];
                    int n2 = numbers[x + 1];
                    if (n1 > n2) {
                        numbers[x + 1] = n1;
                        numbers[x] = n2;

                    }
                    steps++;
                    x++;
                }
                indexUnrevealdNumber++;
            }
        }

        for (int num : numbers) {
            MyConsole console = new MyConsole();
            console.print(num + ", ");
        }
    }

    private static boolean checkSorting(int[] numbers) {
        for (int i : numbers) {
            int before = 0;
            int after = 0;
            int index = Arrays.asList(numbers).indexOf(i);
            if (index != 0) {
                before = numbers[index--];
            }
            if (index != numbers.length) {
                after = numbers[index++];
            }
            if (index < before || index > after) {
                return false;
            }
        }
        return true;
    }
}
