package hugo.io.tbz.M411.sorting.BubbleSort;

import hugo.io.other.cosoleUtility.myconsole.MyConsole;

import java.util.Random;

public class MainBubbleSort {

    public static void main(String[] args) {

        int[] numbers = new int[6];
        numbers[0] = 20;
        numbers[1] = 3;
        numbers[2] = 17;
        numbers[3] = 4;
        numbers[4] = 12;
        numbers[5] = 2;

        int[] numbers5k = new int[5000];
        Random random = new Random();
        for (int i = 0; i < numbers5k.length; i++) {
            numbers5k[i] = random.nextInt();
        }

        int[] numbers10k = new int[10000];
        for (int i = 0; i < numbers10k.length; i++) {
            numbers10k[i] = random.nextInt();
        }

        Long timeStart5 = System.currentTimeMillis();
        int numPasstrough5 = bubblesort(numbers);
        Long timeEnd5 = System.currentTimeMillis();
        Long time5 = timeEnd5 - timeStart5;


        Long timeStart5k = System.currentTimeMillis();
        int numPasstrough5k = bubblesort(numbers5k);
        Long timeEnd5k = System.currentTimeMillis();
        Long time5k = timeEnd5k - timeStart5k;


        Long timeStart10k = System.currentTimeMillis();
        int numPasstrought10k = bubblesort(numbers10k);
        Long timeEnd10k = System.currentTimeMillis();
        Long time10k = timeEnd10k - timeStart10k;

        MyConsole console = new MyConsole();
        console.printLine("Sorting arrays with bubblesort");
        console.printLine("Array of 5 = " + time5 + " millisecond(s), " + "in " + numPasstrough5 + " passthrough");
        console.printLine("Array of 5000 = " + time5k + " milliseconds" + " in " + numPasstrough5k + " passthrough");
        console.printLine("Array of 10000 = " + time10k + " milliseconds" + " in " + numPasstrought10k + " passtrought");
    }

    public static int bubblesort(int[] a) {

        int numPasstrought = 0;
        int lastSwap = a.length - 1;

        for (int i = 1; i < a.length; i++) {
            boolean isSorted = true;
            int currentSwap = -1;

            for (int j = 0; j < lastSwap; j++) {
                isSorted = true;
                if (a[j] > a[j + 1]) {
                    int temp = a[j];
                    a[j] = a[j + 1];
                    a[j + 1] = temp;
                    isSorted = false;
                    currentSwap = j;
                    numPasstrought++;
                }
            }

            if (isSorted) {
                MyConsole console = new MyConsole();
                if (a.length < 50) {
                    for (Integer y : a) {
                        console.print(y + ", ");
                    }
                }
                return numPasstrought;
            }
            lastSwap = currentSwap;
        }
        return numPasstrought;
    }
}
