package hugo.io.tbz.M411.linkedList;

import hugo.io.other.cosoleUtility.myconsole.MyConsole;

public class LinkedList {

    //reference to the head node
    private Node head;
    private int listCount;  //counter used for looping
    //constructor
    public LinkedList() {
        //when initialised it's an empty list, so the reference to the
        //head node is set to a new node with no data:
        head = new Node(null);
        listCount = 0;
    }

    public Node getHead() {
        return head;
    }

    public void setHead(Node head) {
        this.head = head;
    }

    public int getListCount() {
        return listCount;
    }

    public void setListCount(int listCount) {
        this.listCount = listCount;
    }

    public void add(Node value) {
        //head is the current node
        //just to be sure we are at the end of the list, we loop through the elements:
        if (head.getItem() == null) {
            setHead(value);
        } else {
            Node current = head;
            while (current.getNext() != null) {
                current = head.getNext();
            }
            //add the new element to the end of the list:
            current.setNext(value);
        }
        //increment list counter:
        setListCount(getListCount() + 1);
    }

    public Boolean add(Node node, int index) {
        if (getHead() == null && index != 0) {
            return Boolean.FALSE;
        }
        Node element = getHead();
        Node nextElement = null;
        int i = 0;
        while (element.hasNext()) {
            nextElement = element.getNext();
            i++;
            if (i == index - 1) {
                // this is the node right before the insertion
                break;
            }
        }
        element.setNext(node);
        while (nextElement != null) {
            node.setNext(nextElement);
            node = nextElement;
            nextElement = nextElement.getNext();
        }
        return true;
    }

    public void showElements() {

        Node node = this.getHead();
        String value = node.getItem().toString();
        while (node.hasNext()) {
            Node nextNode = node.getNext();
            value = nextNode.getItem().toString() + value;
            node = nextNode;
        }
        MyConsole console = new MyConsole();
        console.printLine(value);
    }

    public void deleteLastElement() {
        Node node = this.getHead();
        while (node.hasNext()) {
            node = node.getNext();
        }
        // this is the last node
        Node nodeBefore = getPreviousNode(node);
        nodeBefore.setNext(null);
    }

    public Node getPreviousNode(Node nodeAfter) {
        Boolean isPrevious = Boolean.FALSE;
        // the first node
        Node nodeBefore = this.getHead();

        while (!isPrevious) {
            if (nodeBefore.getNext().equals(nodeAfter)) {
                isPrevious = Boolean.TRUE;
            } else {
                nodeBefore = nodeBefore.getNext();
            }
        }
        return nodeBefore;
    }
}