package hugo.io.tbz.M411.linkedList;

import hugo.io.other.cosoleUtility.myconsole.MyConsole;

public class Main {

    public static void main(String[] args) {
        LinkedList linkedList = new LinkedList();
        MyConsole console = new MyConsole();
        console.printLine("Add word to the LinkedList");
        String word = console.readLineString();
        Node wordNode = new Node(word);
        linkedList.add(wordNode);

        console.printLine("Add number to the LinkedList");
        Integer number = console.readLineInt();
        Node num = new Node(number.toString());
        linkedList.add(num);

        console.printLine();
        console.printLine("All elements");
        linkedList.showElements();

        console.printLine();
        linkedList.deleteLastElement();
        console.printLine("We deleted the last element");
        linkedList.showElements();

        console.printLine();
        console.printLine("Insert the word");
        String value = console.readLineString();
        console.printLine("Select position, in whitch your word should be inserted");
        int pos = console.readLineInt();
        linkedList.add(new Node(value), pos);
        linkedList.showElements();
    }
}
