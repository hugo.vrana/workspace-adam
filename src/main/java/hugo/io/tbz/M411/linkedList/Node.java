package hugo.io.tbz.M411.linkedList;

public class Node {
    private String item;
    private Node next;

    //constructor
    public Node(String value) {
        next = null;
        item = value;
    }

    //another constructor
    public Node(String value, Node nextValue) {
        next = nextValue;
        item = value;
    }

    public Object getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public Node getNext() {
        return next;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    public Boolean hasNext() {
        try {
            Node nextNode = getNext();
            if (nextNode == null) {
                return Boolean.FALSE;
            }
        } catch (Exception ex) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }
}
