package hugo.io.tbz.M411.primenumber;

public class Main {

    public static void main(String[] argv) {
        for (Integer n = 2; n < Integer.MAX_VALUE; n++) {
            Integer count = 2;
            Boolean value = Boolean.TRUE;
            while (count < n) {
                if ((n % count) == 0) {
                    value = Boolean.FALSE;
                }
                count++;
            }
            if (value) {
                System.out.println(n);
            }
        }
    }
}