package hugo.io.tbz.M411.recursion;

import java.util.Scanner;

public class Main {
    public static int first = 0;
    public static int second = 0;

    public static void main(String[] args) {
        if (first == 0) {
            System.out.println("Enter first number: ");
            Scanner in = new Scanner(System.in);
            first = in.nextInt();
        } else {
            System.out.println("Enter second number: ");
            Scanner in = new Scanner(System.in);
            second = in.nextInt();
        }
        if (second == 0) {
            main(args);
        } else {
            System.out.println(first + " " + second);
        }
    }
}