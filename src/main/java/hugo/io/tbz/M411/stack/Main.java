package hugo.io.tbz.M411.stack;

public class Main {
    public static void main(String[] args) {
        Stack list = new Stack();
        list.push("a");
        list.push("b");
        list.push("c");
        list.push("b");
        list.push("a");
        list.print();
        System.out.println("Peek =" + list.peek().getItem());
        // if else with lambda
        System.out.println((list.isPalindrome() == false) ? "Is not palindrome" : "Is palindrome");

        // Adding elements in the first position
        Stack stack = new Stack();
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.print();
        stack.printReversed();
    }
}
