package hugo.io.tbz.M411.stack;

public class Stack {

    private Node first = new Node(null);
    private int listCount;

    public Stack() {
        listCount = 0;
    }

    public Node getFirst() {
        return first;
    }

    public void push(Object value) {
        // the new element is the last in the list:
        Node old = first;
        first = new Node(value);
        first.setNext(old);
        listCount++;
    }

    public Object pop() {
        Object value = first.getItem();
        first = first.getNext();
        listCount--;
        return value;
    }

    public void print() {
        if (!isEmpty()) {
            Node node = first;
            String s = "";
            while (node.hasNext()) {
                s += node.getItem().toString() + ",";
                node = node.getNext();
            }
            // this is the last element
            s += node.getItem().toString();
            System.out.println(s);
        } else {
            System.out.println("The list is empty");
        }
    }

    public void printReversed() {
        if (!isEmpty()) {
            Node node = first;
            String s = "";
            while (node.hasNext()) {
                s = node.getItem().toString() + "," + s;
                node = node.getNext();
            }
            // this is the last element
            s = node.getItem().toString() + "," + s;
            System.out.println("In reversed: " + s);
        } else {
            System.out.println("The list is empty");
        }
    }

    public Boolean isEmpty() {
        if (!first.hasNext()) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    public Node peek() {
        if (!isEmpty()) {
            Object n = pop();
            push(n);
            Node node = new Node(n);
            return node;
        }
        return null;
    }

    public boolean isPalindrome() {
        if (!isEmpty()) {
            Node n = getFirst();
            String str = n.getItem().toString();
            while (n.next() != null) {
                str += n.next().toString();
                n = n.getNext();
            }
            return str.equals(new StringBuilder(str).reverse().toString());
        }
        return Boolean.FALSE;
    }
}