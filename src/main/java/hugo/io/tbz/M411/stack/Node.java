package hugo.io.tbz.M411.stack;

import java.util.Iterator;

public class Node implements Iterator {
    private Object item;
    private Node next;

    //constructor
    public Node(Object value) {
        next = null;
        item = value;
    }

    //another constructor
    public Node(String value, Node nextValue) {
        next = nextValue;
        item = value;
    }

    @Override
    public boolean hasNext() {
        if (next() == null) {
            return Boolean.FALSE;
        } else {
            return Boolean.TRUE;
        }
    }

    @Override
    public Object next() {
        return getNext().getItem();
    }

    public Object getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public Node getNext() {
        return next;
    }

    public void setNext(Node next) {
        this.next = next;
    }

}
