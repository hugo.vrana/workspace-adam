package hugo.io.tbz.M411.hashMapAirports;

import hugo.io.other.cosoleUtility.myconsole.MyConsole;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        MyConsole console = new MyConsole();
        Map<String, String> airports = new HashMap<>();

        // this file can be found in the res file
        String fileName = "airports.xls";
        try {
            String inputLine;
            String filePath = "res/" + fileName;
            BufferedReader br = new BufferedReader(new FileReader(filePath));
            while ((inputLine = br.readLine()) != null) {
                String[] lineParts = inputLine.split(",");
                airports.put(lineParts[0], lineParts[1]);
            }
        } catch (Exception ex) {
            console.printLine("Exception: " + ex.getMessage());
        }

        airports.forEach((k, v) -> console.printLine("Code: " + k + "; Airport: " + v + ";"));

        console.printLine("Enter Airport Code:");
        String code = console.readLineString();

        Map<String, String> sortedAirports = airports
                .entrySet()
                .stream()
                .filter(p -> p.getKey().matches(code))
                .collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue()));

        sortedAirports.forEach((k, v) -> console.printLine("Code: " + k + "; Airports: " + v + ";"));
    }
}
