package hugo.io.tbz.M226a.I1;

import hugo.io.azo.calc.Calc;

import java.util.ArrayList;

/**
 * Created by hugo on 01.05.17.
 */
public class Main {
    public static void main(String[] args) {

        ArrayList<String> mathProblems;
        mathProblems = new ArrayList<>();

        mathProblems.add("1+1");
        mathProblems.add("9+2");
        mathProblems.add("4-5");
        mathProblems.add("1-9");
        mathProblems.add("5*7");
        mathProblems.add("6*6");
        mathProblems.add("8/2");
        mathProblems.add("9/2");

        for (String item : mathProblems) {
            Calc calc = Calc.getInstance();

            Double result = Double.valueOf(0);

            Character firstNr = item.charAt(0);
            Integer first = Character.getNumericValue(firstNr);
            Long firstValue = first.longValue();
            calc.firstValue(firstValue);

            Character secondNr = item.charAt(2);
            Integer second = Character.getNumericValue(secondNr);
            Long secondValue = second.longValue();
            calc.secondValue(secondValue);

            Character interfunktion = item.charAt(1);

            switch (interfunktion) {

                case '+':
                    calc.operation("+");
                    result = calc.calculate();
                    System.out.println(item + "=" + result);
                    break;

                case '-':
                    calc.operation("-");
                    calc.calculate();
                    result = calc.calculate();
                    System.out.println(item + "=" + result);
                    break;

                case '*':
                    calc.operation("*");
                    result = calc.calculate();
                    System.out.println(item + "=" + result);
                    break;

                case '/':
                    calc.operation("/");
                    result = calc.calculate();
                    System.out.println(item + "=" + result);
                    break;

                default:
                    System.out.println("Error. No interfunktion found.");
                    break;
            }
        }
    }

}
