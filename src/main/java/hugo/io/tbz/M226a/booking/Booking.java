package hugo.io.tbz.M226a.booking;

import hugo.io.other.cosoleUtility.myconsole.MyConsole;

import java.util.ArrayList;

public class Booking {

    private String name;
    private Art art;
    private Float sum;
    private String date;

    private static Booking getInsatnce() {
        return new Booking();
    }

    public static Booking initializeBooking() {
        MyConsole console = new MyConsole();
        Booking booking = getInsatnce();

        console.printLine("Insert the booking's name");
        String name = console.readLineString();
        booking.setName(name);

        ArrayList<String> inOrOut = new ArrayList<String>();
        inOrOut.add("In");
        inOrOut.add("Out");

        console.printMenu(inOrOut);
        int art = console.readLineInt();

        switch (art) {
            case 1:
                booking.setArt(Art.IN);
                break;
            case 2:
                booking.setArt(Art.OUT);
                break;
            default:
                console.print("Invalid position");
        }

        console.printLine("Insert the sum of booking");
        float sum = console.readLineFloat();
        booking.setSum(sum);

        console.printLine("Set the date of booking");
        String date = console.readLineString();
        booking.setDate(date);

        return booking;
    }

    public Art getArt() {
        return art;
    }

    public void setArt(Art art) {
        this.art = art;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getSum() {
        return sum;
    }

    public void setSum(Float sum) {
        this.sum = sum;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void bookIn() {
        this.setArt(Art.IN);
    }

    private void bookOut() {
        this.setArt(Art.OUT);
    }

    public String getInfos() {
        String bookingInfos = this.getName();
        bookingInfos += ", Art: " + this.getArt();
        bookingInfos += ", Sum: " + this.getSum();
        bookingInfos += ", Date:  " + this.getDate();
        return bookingInfos;
    }
}
