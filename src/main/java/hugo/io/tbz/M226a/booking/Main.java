package hugo.io.tbz.M226a.booking;

import hugo.io.other.cosoleUtility.myconsole.MyConsole;

import java.util.ArrayList;

public class Main {

    private static MyConsole console = new MyConsole();
    private static Kassenbuch kassenbuch = Kassenbuch.getInstance();
    private static ArrayList<String> menu = new ArrayList<>();

    public static void main(String[] args) {

        menu.add("Add booking");
        menu.add("Remove booking");
        menu.add("Show all bookings");
        menu.add("Book-In");
        menu.add("Book-Out");
        menu.add("Close");

        while (true) {
            program();
        }
    }

    public static void program() {
        console.printMenu(menu);
        Integer choosen = console.readLineInt();

        switch (choosen) {
            case 1:
                kassenbuch.addBooking();
                break;
            case 2:
                kassenbuch.deleteBooking(console);
                break;
            case 3:
                kassenbuch.showAllBookings(console);
                break;
            case 4:
                kassenbuch.bookIn(console);
                break;
            case 5:
                kassenbuch.bookOut(console);
                break;
            case 6:
                System.exit(0);
                break;
            default:
                break;
        }
    }
}
