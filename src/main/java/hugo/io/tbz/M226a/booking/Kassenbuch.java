package hugo.io.tbz.M226a.booking;

import hugo.io.other.cosoleUtility.myconsole.MyConsole;

import java.util.ArrayList;

public class Kassenbuch {

    private ArrayList<Booking> bookings = new ArrayList<Booking>();

    public static Kassenbuch getInstance() {
        return new Kassenbuch();
    }

    public ArrayList<Booking> getBookings() {
        return bookings;
    }

    public void setBookings(ArrayList<Booking> bookings) {
        this.bookings = bookings;
    }

    public void addBooking() {
        Booking booking = Booking.initializeBooking();
        bookings.add(booking);

    }

    public void deleteBooking(MyConsole console) {
        if (!bookings.isEmpty()) {
            showAllBookings(console);
            console.printLine("Choose the position to delete");
            int pos = console.readLineInt();
            int realPosition = pos - 1;
            try {
                bookings.remove(realPosition);
            } catch (Exception ex) {
                console.printLine("The position you entered doesn't exist.");
            }
        } else {
            console.printLine("Your list is empty.");
            return;
        }
    }

    public void showAllBookings(MyConsole console) {

        if (!bookings.isEmpty()) {
            console.printLine("");
            for (int i = 0; i < bookings.size(); i++) {
                int y = i;
                y++;
                Booking booking = bookings.get(i);
                String infos = booking.getInfos();
                console.printLine(y + " : " + infos);
            }
            console.printLine("");
        } else {
            console.printLine("Your list is empty.");
            return;
        }
    }

    public void bookIn(MyConsole console) {
        if (!bookings.isEmpty()) {
            console.printLine("");

            for (int i = 0; i < bookings.size(); i++) {
                int y = i;
                y++;
                Booking booking = bookings.get(i);

                if (booking.getArt() == Art.IN) {
                    String infos = booking.getInfos();
                    console.printLine(y + " : " + infos);
                }

                console.printLine("");
            }
        } else {
            console.printLine("Your list is empty.");
            return;
        }

        console.printLine("Choose position to book in");
        Integer pos = console.readLineInt();
        try {
            Booking booking = bookings.get(pos);
            booking.setArt(Art.IN);
        } catch (Exception ex) {
            console.printLine("This position is not valid.");
        }
    }

    public void bookOut(MyConsole console) {
        if (!bookings.isEmpty()) {
            console.printLine("");

            for (int i = 0; i < bookings.size(); i++) {
                int y = i;
                y++;
                Booking booking = bookings.get(i);

                if (booking.getArt() == Art.OUT) {
                    String infos = booking.getInfos();
                    console.printLine(y + " : " + infos);
                }
                console.printLine("");
            }
        } else {
            console.printLine("Your list is empty.");
            return;
        }

        console.printLine("Choose position to book in");
        Integer pos = console.readLineInt();
        try {
            Booking booking = bookings.get(pos);
            booking.setArt(Art.IN);
        } catch (Exception ex) {
            console.printLine("This position is not valid.");
        }
    }
}