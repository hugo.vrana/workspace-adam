package hugo.io.tbz.M226a.cli;

import hugo.io.tbz.M226a.cli.questions.Question;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hugo on 28.04.17.
 */
public class QuestionFlow {
    private List<Question> flow;
    private Integer attempts;

    private QuestionFlow() {
        this.flow = new ArrayList<>();
        this.attempts = 3;
    }

    public static QuestionFlow getInstance() {
        return new QuestionFlow();
    }

    public QuestionFlow attempts(Integer attempts) {
        this.attempts = attempts;
        return this;
    }

    public QuestionFlow addQuestion(Question question) {
        this.flow.add(question);
        return this;
    }

    public QuestionFlow start() throws IOException {
        for (Question question : this.flow) {

            for (int i = 1; i <= this.attempts; i++) {
                System.out.println(question.getPrompt());
                BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
                if (question.answer(reader.readLine()).validate()) {
                    break;
                } else {
                    System.out.println(question.getValidationPrompt());
                }
                if (i >= this.attempts) {
                    System.out.println("You have run out with allowed attempts to enter a valid value.");
                    System.out.println("Program has finished.");
                    System.exit(0);
                }
            }


        }
        return this;
    }

    public List<Question> getFlow() {
        return flow;
    }
}
