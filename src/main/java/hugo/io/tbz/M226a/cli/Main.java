package hugo.io.tbz.M226a.cli;

import hugo.io.tbz.M226a.cli.questions.QuestionBoolean;
import hugo.io.tbz.M226a.cli.questions.QuestionSelect;
import hugo.io.tbz.M226a.cli.questions.QuestionValueDate;
import hugo.io.tbz.M226a.cli.questions.QuestionValueNumeric;

import java.io.IOException;

/**
 * Created by hugo on 28.04.17.
 */
public class Main {
    public static void main(String[] args) throws IOException {
        QuestionFlow questionFlow = QuestionFlow.getInstance();
        questionFlow
                .attempts(4)
                .addQuestion(QuestionBoolean.getInstance().prompt("Do you want to be free?"))
                .addQuestion(QuestionSelect.getInstance()
                        .addOption("+", "plus")
                        .addOption("-", "minus")
                        .addOption("*", "multiply")
                        .addOption("/", "divide")
                        .prompt("Choose operation")
                )
                .addQuestion(QuestionValueNumeric.getInstance().prompt("What is your age?"))
                .addQuestion(QuestionValueDate.getInstance().prompt("What is today's date? (yyyy-MM-dd)"))
                .addQuestion(QuestionValueNumeric.getInstance().prompt("What is the value of 5+4 ?"))
        ;
        questionFlow.start();
    }
}
