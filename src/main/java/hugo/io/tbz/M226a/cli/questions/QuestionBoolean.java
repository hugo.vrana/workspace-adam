package hugo.io.tbz.M226a.cli.questions;

/**
 * Created by hugo on 28.04.17.
 */
public class QuestionBoolean extends Question {
    private QuestionBoolean() {
        super();
        addOption("y", "yes");
        addOption("n", "no");
    }

    public static QuestionBoolean getInstance() {
        return new QuestionBoolean();
    }

    @Override
    public Boolean validate() {
        Boolean result = Boolean.FALSE;
        if (getOptions().keySet().contains(getAnswer())) {
            return Boolean.TRUE;
        }
        setValidationPrompt("Entered value " + getAnswer() + " is not in " + getOptions().toString());
        return result;
    }
}
