package hugo.io.tbz.M226a.cli.questions;

/**
 * Created by hugo on 28.04.17.
 */
public class QuestionValueNumeric extends Question {
    private QuestionValueNumeric() {
        super();
    }

    public static QuestionValueNumeric getInstance() {
        return new QuestionValueNumeric();
    }

    @Override
    public Boolean validate() {
        Boolean result = Boolean.TRUE;
        if (getAnswer() == null || "".equals(getAnswer())) {
            setValidationPrompt("Entered value  is empty");
            return Boolean.FALSE;
        }
        for (char c : getAnswer().toCharArray()) {
            if (c < '0' || c > '9') {
                setValidationPrompt("Entered value " + getAnswer() + " is not a number");
                return Boolean.FALSE;
            }
        }
        return result;
    }
}
