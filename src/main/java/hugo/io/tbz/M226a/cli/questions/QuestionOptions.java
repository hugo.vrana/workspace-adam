package hugo.io.tbz.M226a.cli.questions;

/**
 * Created by hugo on 28.04.17.
 */
public interface QuestionOptions {

    String getCode();

    String getName();

}
