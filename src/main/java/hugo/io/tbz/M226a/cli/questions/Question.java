package hugo.io.tbz.M226a.cli.questions;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by hugo on 28.04.17.
 */
public abstract class Question {

    private String prompt;
    private String answer;
    private String validationPrompt;
    private Map<String, String> options;

    protected Question() {
        this.options = new HashMap<>();
    }

    public Question prompt(String prompt) {
        this.prompt = (this.options.isEmpty()) ? prompt : prompt + " " + this.options.toString();
        return this;
    }

    public String getPrompt() {
        return prompt;
    }

    public Question answer(String answer) {
        this.answer = answer;
        return this;
    }

    public String getAnswer() {
        return answer;
    }

    public Question addOptions(QuestionOptions[] questionOptions) {
        for (QuestionOptions value : questionOptions) {
            this.options.put(value.getCode(), value.getName());
        }
        return this;
    }

    public Question addOption(String key, String value) {
        this.options.put(key, value);
        return this;
    }

    public Map<String, String> getOptions() {
        return options;
    }

    public String getValidationPrompt() {
        return validationPrompt;
    }

    public void setValidationPrompt(String validationPrompt) {
        this.validationPrompt = validationPrompt;
    }

    public abstract Boolean validate();
}
