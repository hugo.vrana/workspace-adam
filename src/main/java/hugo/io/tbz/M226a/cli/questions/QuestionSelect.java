package hugo.io.tbz.M226a.cli.questions;

/**
 * Created by hugo on 28.04.17.
 */
public class QuestionSelect extends Question {
    private QuestionSelect() {
        super();
    }

    public static QuestionSelect getInstance() {
        return new QuestionSelect();
    }

    @Override
    public Boolean validate() {
        Boolean result = Boolean.FALSE;
        if (getOptions().keySet().contains(getAnswer())) {

            return Boolean.TRUE;
        }
        setValidationPrompt("Entered value" + getAnswer() + " is not in " + getOptions().toString());
        return result;
    }
}
