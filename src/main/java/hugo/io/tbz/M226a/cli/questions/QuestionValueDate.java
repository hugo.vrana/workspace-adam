package hugo.io.tbz.M226a.cli.questions;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Objects;


/**
 * Created by hugo on 30.04.17.
 */
public class QuestionValueDate extends Question {

    private QuestionValueDate() {
        super();
    }

    public static QuestionValueDate getInstance() {
        return new QuestionValueDate() {
        };
    }

    public Boolean validate() {
        if (getAnswer() == null || "".equals(getAnswer())) {
            System.out.println("Entered value  is empty");
            return Boolean.FALSE;
        }

        if (Objects.equals(LocalDate.now(ZoneId.of("Europe/Paris")).toString(), getAnswer())) {
            System.out.println("Entered value " + getAnswer() + " is a date");
        } else {
            System.out.println("Entered value " + getAnswer() + " is not a date");
        }

        return Boolean.TRUE;
    }
}
