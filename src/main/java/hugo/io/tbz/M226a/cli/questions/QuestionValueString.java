package hugo.io.tbz.M226a.cli.questions;

/**
 * Created by hugo on 28.04.17.
 */
public class QuestionValueString extends Question {
    private QuestionValueString() {
        super();
    }

    public static QuestionValueString getInstance() {
        return new QuestionValueString();
    }

    @Override
    public Boolean validate() {
        Boolean result = Boolean.FALSE;
        if (getAnswer() != null && !"".equals(getAnswer())) {
            return Boolean.TRUE;
        }
        setValidationPrompt("Entered value " + getAnswer() + " is empty");
        return result;
    }
}