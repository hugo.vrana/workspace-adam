package hugo.io.tbz.M226a.sportschef_v2;

public class Ersatzspieler {

    private String name;

    public static Ersatzspieler getInstance() {
        Ersatzspieler ersatzspieler = new Ersatzspieler();
        ersatzspieler.setName("Ersatzspieler");
        return ersatzspieler;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
