package hugo.io.tbz.M226a.sportschef_v2;

import hugo.io.other.cosoleUtility.myconsole.MyConsole;

import java.util.ArrayList;

public class Application {

    private ArrayList<Team> teams = new ArrayList<>();

    public ArrayList<Team> getTeams() {
        return teams;
    }

    public void setTeams(ArrayList<Team> teams) {
        this.teams = teams;
    }

    public void main() {
        ArrayList<String> menu = new ArrayList<>();
        menu.add("Add Team");
        menu.add("Show one team");
        menu.add("Show all teams");

        MyConsole console = new MyConsole();
        console.printNumberedList(menu);
        Integer chosen = console.readLineInt();
        switch (chosen) {
            case 1:
                ArrayList<Team> teams = getTeams();
                Team newTeam = createTeam();
                teams.add(newTeam);
                setTeams(teams);
                break;
            case 2:
                ArrayList<Team> teams1 = getTeams();
                if (!teams1.isEmpty()) {
                    for (Team t : teams1) {
                        console.printLine(teams1.indexOf(t) + ". " + t.getName());
                    }
                    console.printLine("Choose your team");
                    chosen = console.readLineInt();
                    Team team = teams1.get(chosen);

                    // printing all names
                    console.printLine("Team name: " + team.getName());
                    console.printLine("Sportschef: " + team.getSportschef().getName());
                    console.printLine("Trainer: " + team.getSportschef().getTrainer().getName());
                    console.printLine("Goalie: " + team.getSportschef().getTrainer().getGoalie().getName());
                    ArrayList<Feldspieler> feldspielers = team.getSportschef().getTrainer().getFeldspielers();
                    for (Feldspieler f : feldspielers) {
                        int index = feldspielers.indexOf(f) + 1;
                        console.printLine("Feldspieler " + index + ": " + f.getName());
                    }
                } else {
                    console.printLine("No teams");
                }
                break;
            case 3:
                ArrayList<Team> teams2 = getTeams();
                if (teams2.isEmpty()) {
                    console.printLine("No teams");
                } else {
                    for (Team t : teams2) {
                        console.printLine(teams2.indexOf(t) + ": " + t.getName());
                    }
                }
                break;
        }
    }

    // creates an functioning team already initialized with names
    private Team createTeam() {
        MyConsole console = new MyConsole();

        Team team = Team.getInstance();
        console.printLine("Team Name");
        String teamName = console.readLineString();
        team.setName(teamName);

        Sportschef sportschef = Sportschef.getInstance();
        console.printLine("Sportschef Name");
        String sportschefName = console.readLineString();
        sportschef.setName(sportschefName);
        team.setSportschef(sportschef);

        Trainer trainer = Trainer.getInstance();
        console.printLine("Trainer Name");
        String trainerName = console.readLineString();
        trainer.setName(trainerName);
        sportschef.setTrainer(trainer);

        Goalie goalie = Goalie.getInstance();
        console.printLine("Goalie Name");
        String goalieName = console.readLineString();
        goalie.setName(goalieName);
        trainer.setGoalie(goalie);

        ArrayList<Feldspieler> feldspielersLocal = trainer.getFeldspielers();

        Feldspieler f1 = Feldspieler.getInstance();
        console.printLine("Feldspieler 1 Name");
        String f1Name = console.readLineString();
        f1.setName(f1Name);
        feldspielersLocal.add(f1);

        Feldspieler f2 = Feldspieler.getInstance();
        console.printLine("Feldspieler 2 Name");
        String f2Name = console.readLineString();
        f2.setName(f2Name);
        feldspielersLocal.add(f2);

        Feldspieler f3 = Feldspieler.getInstance();
        console.printLine("Feldspieler 3 Name");
        String f3Name = console.readLineString();
        f3.setName(f3Name);
        feldspielersLocal.add(f1);

        Feldspieler f4 = Feldspieler.getInstance();
        console.printLine("Feldspieler 4 Name");
        String f4Name = console.readLineString();
        f4.setName(f4Name);
        feldspielersLocal.add(f4);

        Feldspieler f5 = Feldspieler.getInstance();
        console.printLine("Feldspieler 5 Name");
        String f5Name = console.readLineString();
        f5.setName(f5Name);
        feldspielersLocal.add(f5);

        Feldspieler f6 = Feldspieler.getInstance();
        console.printLine("Feldspieler 6 Name");
        String f6Name = console.readLineString();
        f6.setName(f6Name);
        feldspielersLocal.add(f1);

        Feldspieler f7 = Feldspieler.getInstance();
        console.printLine("Feldspieler 7 Name");
        String f7Name = console.readLineString();
        f7.setName(f7Name);
        feldspielersLocal.add(f7);

        Feldspieler f8 = Feldspieler.getInstance();
        console.printLine("Feldspieler 8 Name");
        String f8Name = console.readLineString();
        f8.setName(f8Name);
        feldspielersLocal.add(f8);

        Feldspieler f9 = Feldspieler.getInstance();
        console.printLine("Feldspieler 9 Name");
        String f9Name = console.readLineString();
        f9.setName(f9Name);
        feldspielersLocal.add(f9);

        Feldspieler f10 = Feldspieler.getInstance();
        console.printLine("Feldspieler 10 Name");
        String f10Name = console.readLineString();
        f10.setName(f10Name);
        feldspielersLocal.add(f10);

        // adding all Feldspielers to trainer
        trainer.setFeldspielers(feldspielersLocal);

        ArrayList<Ersatzspieler> ersatzspielersLocal = trainer.getErsatzspielers();

        Ersatzspieler ersatzspieler1 = Ersatzspieler.getInstance();
        console.printLine("Ersatzbank 1 Name");
        String e1Name = console.readLineString();
        ersatzspieler1.setName(e1Name);
        ersatzspielersLocal.add(ersatzspieler1);

        Ersatzspieler ersatzspieler2 = Ersatzspieler.getInstance();
        console.printLine("Ersatzbank 2 Name");
        String e2Name = console.readLineString();
        ersatzspieler2.setName(e2Name);
        ersatzspielersLocal.add(ersatzspieler2);

        Ersatzspieler ersatzspieler3 = Ersatzspieler.getInstance();
        console.printLine("Ersatzbank 3 Name");
        String e3Name = console.readLineString();
        ersatzspieler3.setName(e3Name);
        ersatzspielersLocal.add(ersatzspieler3);

        // Adding all ersatzspielers to trainer
        trainer.setErsatzspielers(ersatzspielersLocal);

        return team;
    }
}