package hugo.io.tbz.M226a.steuerungMitInterationsvarianten;

/**
 * Created by hvran on 10.03.2017.
 */
public class Main {
    public static void main(String[] Args) {
        float anfang = 5;
        int anzahl = 0;

        while (anfang > 0) {
            while (anzahl <= 20) {
                System.out.print("ha ");
                anzahl++;
            }
            anzahl = 0;
            while (anzahl <= 5) {
                System.out.print("hi ");
                anzahl++;
            }
            anzahl = 0;
            anfang--;
        }
    }
}