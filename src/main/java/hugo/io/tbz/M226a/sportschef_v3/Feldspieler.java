package hugo.io.tbz.M226a.sportschef_v3;

public class Feldspieler {

    private String name;

    public Feldspieler() {

    }

    public Feldspieler(String name) {
        this.setName(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
