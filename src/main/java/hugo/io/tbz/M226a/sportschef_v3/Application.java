package hugo.io.tbz.M226a.sportschef_v3;

import hugo.io.other.cosoleUtility.myconsole.MyConsole;

import java.util.ArrayList;

public class Application {

    private ArrayList<Team> teams = new ArrayList<>();

    public ArrayList<Team> getTeams() {
        return teams;
    }

    public void setTeams(ArrayList<Team> teams) {
        this.teams = teams;
    }

    public void main() {
        ArrayList<String> menu = new ArrayList<>();
        menu.add("Add Team");
        menu.add("Show one team");
        menu.add("Show all teams");

        MyConsole console = new MyConsole();
        console.printNumberedList(menu);
        Integer chosen = console.readLineInt();
        switch (chosen) {
            case 1:
                ArrayList<Team> teams = getTeams();
                Team newTeam = createTeam();
                teams.add(newTeam);
                setTeams(teams);
                break;
            case 2:
                ArrayList<Team> teams1 = getTeams();
                if (!teams1.isEmpty()) {
                    for (Team t : teams1) {
                        console.printLine(teams1.indexOf(t) + ". " + t.getName());
                    }
                    console.printLine("Choose your team");
                    chosen = console.readLineInt();
                    Team team = teams1.get(chosen);

                    // printing all names
                    console.printLine("Team name: " + team.getName());
                    console.printLine("Sportschef: " + team.getSportschef().getName());
                    console.printLine("Trainer: " + team.getSportschef().getTrainer().getName());
                    console.printLine("Goalie: " + team.getSportschef().getTrainer().getGoalie().getName());
                    ArrayList<Feldspieler> feldspielers = team.getSportschef().getTrainer().getFeldspielers();
                    for (Feldspieler f : feldspielers) {
                        int index = feldspielers.indexOf(f) + 1;
                        console.printLine("Feldspieler " + index + ": " + f.getName());
                    }
                    ArrayList<Ersatzspieler> ersatzspielers = team.getSportschef().getTrainer().getErsatzspielers();
                    for (Ersatzspieler e : ersatzspielers) {
                        int index = feldspielers.indexOf(e) + 1;
                        console.printLine("Ersatzspieler " + index + ": " + e.getName());
                    }
                } else {
                    console.printLine("No teams");
                }
                break;
            case 3:
                ArrayList<Team> teams2 = getTeams();
                if (teams2.isEmpty()) {
                    console.printLine("No teams");
                } else {
                    for (Team t : teams2) {
                        console.printLine(teams2.indexOf(t) + ": " + t.getName());
                    }
                }
                break;
        }
    }

    // creates an functioning team already initialized with names
    private Team createTeam() {
        MyConsole console = new MyConsole();

        console.printLine("Team Name");
        String teamName = console.readLineString();
        Team team = new Team(teamName);

        console.printLine("Sportschef Name");
        String sportschefName = console.readLineString();
        Sportschef sportschef = new Sportschef(sportschefName);
        team.setSportschef(sportschef);


        console.printLine("Trainer Name");
        String trainerName = console.readLineString();
        Trainer trainer = new Trainer(trainerName);
        sportschef.setTrainer(trainer);

        console.printLine("Goalie Name");
        String goalieName = console.readLineString();
        Goalie goalie = new Goalie(goalieName);
        trainer.setGoalie(goalie);

        ArrayList<Feldspieler> feldspielersLocal = trainer.getFeldspielers();

        console.printLine("Feldspieler 1 Name");
        String f1Name = console.readLineString();
        Feldspieler f1 = new Feldspieler(f1Name);
        feldspielersLocal.add(f1);

        console.printLine("Feldspieler 2 Name");
        String f2Name = console.readLineString();
        Feldspieler f2 = new Feldspieler(f2Name);
        feldspielersLocal.add(f2);

        console.printLine("Feldspieler 3 Name");
        String f3Name = console.readLineString();
        Feldspieler f3 = new Feldspieler(f3Name);
        feldspielersLocal.add(f1);

        console.printLine("Feldspieler 4 Name");
        String f4Name = console.readLineString();
        Feldspieler f4 = new Feldspieler(f4Name);
        feldspielersLocal.add(f4);


        console.printLine("Feldspieler 5 Name");
        String f5Name = console.readLineString();
        Feldspieler f5 = new Feldspieler(f5Name);
        feldspielersLocal.add(f5);


        console.printLine("Feldspieler 6 Name");
        String f6Name = console.readLineString();
        Feldspieler f6 = new Feldspieler(f6Name);
        feldspielersLocal.add(f6);

        console.printLine("Feldspieler 7 Name");
        String f7Name = console.readLineString();
        Feldspieler f7 = new Feldspieler(f7Name);
        feldspielersLocal.add(f7);


        console.printLine("Feldspieler 8 Name");
        String f8Name = console.readLineString();
        Feldspieler f8 = new Feldspieler(f8Name);
        feldspielersLocal.add(f8);


        console.printLine("Feldspieler 9 Name");
        String f9Name = console.readLineString();
        Feldspieler f9 = new Feldspieler(f9Name);
        feldspielersLocal.add(f9);


        console.printLine("Feldspieler 10 Name");
        String f10Name = console.readLineString();
        Feldspieler f10 = new Feldspieler(f10Name);
        feldspielersLocal.add(f10);

        // adding all Feldspielers to trainer
        trainer.setFeldspielers(feldspielersLocal);

        ArrayList<Ersatzspieler> ersatzspielersLocal = trainer.getErsatzspielers();

        console.printLine("Ersatzbank 1 Name");
        String e1Name = console.readLineString();
        Ersatzspieler ersatzspieler1 = new Ersatzspieler(e1Name);
        ersatzspielersLocal.add(ersatzspieler1);

        console.printLine("Ersatzbank 2 Name");
        String e2Name = console.readLineString();
        Ersatzspieler ersatzspieler2 = new Ersatzspieler(e2Name);
        ersatzspielersLocal.add(ersatzspieler2);

        console.printLine("Ersatzbank 3 Name");
        String e3Name = console.readLineString();
        Ersatzspieler ersatzspieler3 = new Ersatzspieler(e3Name);
        ersatzspielersLocal.add(ersatzspieler3);

        // Adding all ersatzspielers to trainer
        trainer.setErsatzspielers(ersatzspielersLocal);

        return team;
    }
}