package hugo.io.tbz.M226a.sportschef_v3;

import java.util.ArrayList;

public class Trainer {

    private String name;
    private ArrayList<Feldspieler> feldspielers = new ArrayList<>();
    private ArrayList<Ersatzspieler> ersatzspielers = new ArrayList<>();
    private Goalie goalie;

    public Trainer(String name) {
        this.setName(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Feldspieler> getFeldspielers() {
        return feldspielers;
    }

    public void setFeldspielers(ArrayList<Feldspieler> feldspielers) {
        this.feldspielers = feldspielers;
    }

    public ArrayList<Ersatzspieler> getErsatzspielers() {
        return ersatzspielers;
    }

    public void setErsatzspielers(ArrayList<Ersatzspieler> ersatzspielers) {
        this.ersatzspielers = ersatzspielers;
    }

    public Goalie getGoalie() {
        return goalie;
    }

    public void setGoalie(Goalie goalie) {
        this.goalie = goalie;
    }
}
