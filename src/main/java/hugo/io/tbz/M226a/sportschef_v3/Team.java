package hugo.io.tbz.M226a.sportschef_v3;

import java.util.ArrayList;

public class Team {

    private String name;
    private Sportschef sportschef;
    private ArrayList<String> menu = new ArrayList<>();

    public Team() {
    }

    public Team(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Sportschef getSportschef() {
        return sportschef;
    }

    public void setSportschef(Sportschef sportschef) {
        this.sportschef = sportschef;
    }
}