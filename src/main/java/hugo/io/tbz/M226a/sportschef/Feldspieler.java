package hugo.io.tbz.M226a.sportschef;

public class Feldspieler {

    private String name;

    public static Feldspieler getInstance() {
        Feldspieler feldspieler = new Feldspieler();
        feldspieler.setName("Feldspieler");
        return feldspieler;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
