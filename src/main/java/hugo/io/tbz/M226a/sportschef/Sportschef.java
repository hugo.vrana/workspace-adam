package hugo.io.tbz.M226a.sportschef;

public class Sportschef {

    private String name;
    private Trainer trainer;

    public static Sportschef getInstance() {
        Sportschef sportschef = new Sportschef();
        sportschef.setName("Sportschef");
        return sportschef;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Trainer getTrainer() {
        return trainer;
    }

    public void setTrainer(Trainer trainer) {
        this.trainer = trainer;
    }
}