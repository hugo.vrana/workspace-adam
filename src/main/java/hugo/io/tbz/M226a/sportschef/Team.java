package hugo.io.tbz.M226a.sportschef;

import java.util.ArrayList;

public class Team {

    private String name;
    private Sportschef sportschef;
    private ArrayList<String> menu = new ArrayList<>();

    public static Team getInstance() {
        Team team = new Team();
        team.setName("Team");
        return team;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Sportschef getSportschef() {
        return sportschef;
    }

    public void setSportschef(Sportschef sportschef) {
        this.sportschef = sportschef;
    }
}