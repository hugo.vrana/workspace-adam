package hugo.io.tbz.M226a.sportschef;

public class Goalie {

    private String name;

    public static Goalie getInstance() {
        return new Goalie();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}