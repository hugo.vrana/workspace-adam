package hugo.io.tbz.M226a.pi;

import hugo.io.other.cosoleUtility.myconsole.MyConsole;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        Double babylonPi = babylon();
        Double egyptPi = egypt();
        Double chinaPi = china();
        Double archymedesPi = archymedes();
        Double javaPI = Math.PI;

        List<String> list = new ArrayList<>();
        list.add("Babylon: " + babylonPi.toString());
        list.add("Egipt: " + egyptPi.toString());
        list.add("China: " + chinaPi.toString());
        list.add("Archidemes: " + archymedesPi.toString());
        list.add("Java: " + javaPI);

        MyConsole console = new MyConsole();
        console.printList(list);
    }

    // 3 1/8
    private static Double babylon() {
        Double beforePoint = Double.valueOf(3);
        Double afterPoint = 1 / Double.valueOf(8);
        Double pi = beforePoint + afterPoint;
        return pi;
    }

    // (16/9)²
    private static Double egypt() {
        Double pi = 16 / Double.valueOf(9);
        pi = Math.pow(pi, 2);
        return pi;
    }

    // √10
    private static Double china() {
        Double chinaPi = Math.sqrt(10);
        return chinaPi;
    }

    // 3 1/7
    private static Double archymedes() {
        Double beforePoint = Double.valueOf(3);
        Double devide = 1 / Double.valueOf(7);
        Double pi = beforePoint + Double.valueOf(devide);
        return pi;
    }
}
