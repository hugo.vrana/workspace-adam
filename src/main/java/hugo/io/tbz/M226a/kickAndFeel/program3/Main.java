package hugo.io.tbz.M226a.kickAndFeel.program3;

import java.util.Random;

/**
 * Created by hvran on 18.03.2017.
 */
public class Main {
    public static void main(String[] args) {
        Random randomGenerator = new Random();
        int randomNumber = 0;
        randomNumber = randomGenerator.nextInt(100);

        if (randomNumber != 0) {
            System.out.println("Generated, not null !");

            if (randomNumber == 20) {
                System.out.println("The number is 20");
            } else if (randomNumber < 20) {
                System.out.println("The number lower than 20");
            } else if (randomNumber > 20) {
                System.out.println("The number is higher than 20");
            }

            System.out.println("The number is " + randomNumber);
        } else  // vorkommt nur falls es ein Fehler beim Generieren gibt.
        {
            System.out.println("Number was not generated or the value is null !");
        }
    }
}