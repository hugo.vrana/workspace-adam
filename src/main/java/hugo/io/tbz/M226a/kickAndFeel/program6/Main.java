package hugo.io.tbz.M226a.kickAndFeel.program6;

import java.util.Random;

/**
 * Created by hvran on 19.03.2017.
 */
public class Main {
    public static void main(String[] args) {
        System.out.println("The program started.");
        int versuche = 0;
        randomNumber();
        while (randomNumber() != 6) {
            versuche++;
            randomNumber();
        }
        System.out.println("Number 6 has been generated");
        System.out.println("You needed " + versuche + " attemps.");
    }

    public static int randomNumber() {
        Random randomGenerator = new Random();
        int random = randomGenerator.nextInt(7);
        return random;
    }
}
