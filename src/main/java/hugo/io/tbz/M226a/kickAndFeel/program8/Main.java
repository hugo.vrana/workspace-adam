package hugo.io.tbz.M226a.kickAndFeel.program8;

/**
 * Created by hvran on 19.03.2017.
 */
public class Main {
    public static void main(String[] args) {
        int versuche = 0;
        boolean noSix = false;
        do {
            versuche += 1;
            int rndNumber = RandomNumber.generate();


            if (rndNumber == 6) {
                System.out.println("Number 6 has been generated");
                System.out.println("You needed " + versuche + " attemp(s).");
                System.out.println("Ende");
                noSix = true;
            }
        }

        while (noSix == false);
    }
}
