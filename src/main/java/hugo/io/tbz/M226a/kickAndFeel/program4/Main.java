package hugo.io.tbz.M226a.kickAndFeel.program4;

import java.util.Random;

/**
 * Created by hvran on 18.03.2017.
 */
public class Main {
    public static void main(String[] args) {
        int min = 1;
        int max = 6;
        Random randomGenerator = new Random();
        int random = randomGenerator.nextInt(max);

        if (random == 3) {
            System.out.println("Gratuliere, du hast eine 3 gewürfelt");
        } else if (random == 4 || random == 5 || random == 6) {
            System.out.println("Du hast eine " + random + " gewürflet und du darfts es nochmall probieren");
            main(args);
        } else {
            System.out.println("Leider verloren, du hast eine " + random);
        }
    }
}
