package hugo.io.tbz.M226a.steuerungMitVariablenUndObjekten;

/**
 * Created by hvran on 10.03.2017.
 */
public class Main {
    public static void main(String[] arguments) {
        int res = ubergabe();
        System.out.print("5 + 6 = " + res);
    }

    public static int ubergabe() {
        int zahl1 = 5;
        int zahl2 = 6;
        int resultat = zahl1 + zahl2;
        return resultat;
    }
}
