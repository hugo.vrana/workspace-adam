package hugo.io.tbz.M326.restaurant;

import java.util.ArrayList;

public class Restaurant {

	// region Properties
	private int id;
	private String name;
	private ArrayList<Employee> employees;
	private ArrayList<Menu> menus;
	private int stars;
    private Boolean open;
    // endregion

    // region Getters / Setters
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<Employee> getEmployees() {
		return this.employees;
	}

	public void setEmployees(ArrayList<Employee> employees) {
		this.employees = employees;
	}

	public ArrayList<Menu> getMenus() {
		return this.menus;
	}

	public void setMenus(ArrayList<Menu> menus) {
		this.menus = menus;
	}

	public int getStars() {
		return this.stars;
	}

	public void setStars(int stars) {
		this.stars = stars;
	}

    public Boolean getOpen() {
        return open;
    }

    public void setOpen(Boolean open) {
        this.open = open;
    }

    // endregion

    // region Constructors
	public Restaurant() {}

	/**
	 * 
	 * @param id
	 * @param name
	 * @param employees
	 * @param menus
	 * @param stars
	 */
	public Restaurant(int id, String name, ArrayList<Employee> employees, ArrayList<Menu> menus, int stars) {
		this.id = id;
		this.name = name;
		this.employees = employees;
		this.menus = menus;
		this.stars = stars;
		this.open = false;
	}
    // endregion

    // region Methods
	public void open() {
		this.open = true;
	}

	public void close() {
		this.open = false;
	}

	/**
	 * 
	 * @param meal
	 * @param menu
	 */
	public void addMealToMenu(Meal meal, Menu menu) {
		menu.getMeals().add(meal);
		menus.add(menu);
	}

	/**
	 * 
	 * @param employee
	 */
	public void addEmployee(Employee employee) {
		employees.add(employee);
	}

	/**
	 * 
	 * @param employee
	 */
	public void removeEmployee(Employee employee) {
		employees.remove(employee);
	}
	// endregion
}