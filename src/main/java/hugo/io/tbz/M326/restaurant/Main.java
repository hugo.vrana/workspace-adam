package hugo.io.tbz.M326.restaurant;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;

public class Main {

    // region Methods
	public static void main(String[] args) {
		Repository repository = new Repository();
        ArrayList<DayOfWeek> workingDays = new ArrayList();
        workingDays.add(DayOfWeek.MONDAY);
        workingDays.add(DayOfWeek.TUESDAY);
        workingDays.add(DayOfWeek.THURSDAY);
        workingDays.add(DayOfWeek.FRIDAY);
        workingDays.add(DayOfWeek.SATURDAY);
        workingDays.add(DayOfWeek.SUNDAY);

        ArrayList<String> workingHours = new ArrayList();
        workingHours.add("8:00 - 12:00");
        workingHours.add("13:00 - 21:00");

        ArrayList<Employee> employeesRestaurantOne = new ArrayList();

        Cook chefCook = new Cook(repository.getNewId(), "Mr", "Peter", "Muster", LocalDateTime.now(), workingDays, workingHours);
        employeesRestaurantOne.add(chefCook);

        Cook secondCook = new Cook(repository.getNewId(), "Mrs", "Anina", "Nachname", LocalDateTime.now(), workingDays, workingHours);
        employeesRestaurantOne.add(secondCook);

        Waiter firstWaiter = new Waiter(repository.getNewId(), "Mr", "Maho", "Jakotic", LocalDateTime.of(1,2,3,4,5), workingDays, workingHours);
        employeesRestaurantOne.add(firstWaiter);

        Waiter secondWaiter = new Waiter(repository.getNewId(), "Mrs", "Sophie", "Jasmine", LocalDateTime.of(5,4,3,3,1), workingDays, workingHours);
        employeesRestaurantOne.add(secondWaiter);

        Servant firstServant = new Servant(repository.getNewId(), "MuDr", "Lea", "Lola", LocalDateTime.of(9,8,7,6,5), workingDays, workingHours);
        employeesRestaurantOne.add(firstServant);

        Servant secondServant = new Servant(repository.getNewId(), "IDK", "Sofiy", "Unbekannt", LocalDateTime.of(10,11,12,13,14), workingDays, workingHours);
        employeesRestaurantOne.add(secondServant);

        ArrayList<Menu> menusRestaurantOne = new ArrayList<>();

        ArrayList<Meal> menuOneMealsRestaurantOne = new ArrayList<>();
        Meal rissotto = new Meal();
        menuOneMealsRestaurantOne.add(rissotto);

        ArrayList<String> ingrediensForGravy = new ArrayList<>();
        ingrediensForGravy.add("Meat");
        ingrediensForGravy.add("Sauce");

        Meal gravy = new Meal(repository.getNewId(), EnumMealType.SOUP, ingrediensForGravy);
        menuOneMealsRestaurantOne.add(gravy);

        Menu menuOne = new Menu(menuOneMealsRestaurantOne, 55.50f);
        menusRestaurantOne.add(menuOne);

        ArrayList<Meal> menuTwoMealsRestaurantOne = new ArrayList<>();
        Meal pancakes = new Meal();
        menuTwoMealsRestaurantOne.add(pancakes);

        ArrayList<String> ingrediensForSpaghettiBolonese = new ArrayList<>();
        ingrediensForGravy.add("Spaghetti");
        ingrediensForGravy.add("Bolognese sauce");

        Meal spaghettiBolonese = new Meal(repository.getNewId(), EnumMealType.MAIN_COURSE, ingrediensForSpaghettiBolonese);
        menuOneMealsRestaurantOne.add(spaghettiBolonese);

        Menu menuTwoRestaurantOne = new Menu(menuTwoMealsRestaurantOne, 12.30f);
        menusRestaurantOne.add(menuTwoRestaurantOne);

        Restaurant restaurantOne = new Restaurant(repository.getNewId(), "Local restaurant" , employeesRestaurantOne,  menusRestaurantOne, 5);
        restaurantOne.setEmployees(employeesRestaurantOne);
        restaurantOne.setMenus(menusRestaurantOne);
        repository.add(restaurantOne);

        ArrayList<Employee> employeesRestaurantTwo = new ArrayList();

        Cook chefCookRestaurantTwo = new Cook(repository.getNewId(), "Mr", "Peter", "Muster", LocalDateTime.of(1990, 12, 13, 00, 00), workingDays, workingHours);
        employeesRestaurantTwo.add(chefCookRestaurantTwo);

        Cook secondCookRestaurantTwo = new Cook(repository.getNewId(), "Mrs", "Anina", "Nachname", LocalDateTime.now(), workingDays, workingHours);
        employeesRestaurantTwo.add(secondCookRestaurantTwo);

        Waiter firstWaiterRestaurantTwo = new Waiter(repository.getNewId(), "Mr", "Maho", "Jakotic", LocalDateTime.of(1,2,3,4,5), workingDays, workingHours);
        employeesRestaurantTwo.add(firstWaiterRestaurantTwo);

        Waiter secondWaiterRestaurantTwo = new Waiter(repository.getNewId(), "Mrs", "Sophie", "Jasmine", LocalDateTime.of(5,4,3,3,1), workingDays, workingHours);
        employeesRestaurantTwo.add(secondWaiterRestaurantTwo);

        Servant firstServantRestaurantTwo = new Servant(repository.getNewId(), "MuDr", "Lea", "Lola", LocalDateTime.of(9,8,7,6,5), workingDays, workingHours);
        employeesRestaurantTwo.add(firstServantRestaurantTwo);

        Servant secondServantRestaurantTwo = new Servant(repository.getNewId(), "IDK", "Sofiy", "Unbekannt", LocalDateTime.of(10,11,12,13,14), workingDays, workingHours);
        employeesRestaurantTwo.add(secondServantRestaurantTwo);

        ArrayList<Menu> menusRestaurantTwo = new ArrayList<>();

        ArrayList<Meal> menuOneMealsRestaurantTwo = new ArrayList<>();
        ArrayList<String> ingredientsForFondue = new ArrayList<>();
        ingredientsForFondue.add("Fondue cheese mix");
        ingredientsForFondue.add("Wine");
        ingredientsForFondue.add("Cooked potatoes");
        Meal fondue = new Meal(repository.getNewId(), EnumMealType.MAIN_COURSE, ingredientsForFondue);
        menuOneMealsRestaurantTwo.add(fondue);

        ArrayList<String> ingrediensForRaclette = new ArrayList<>();
        ingrediensForRaclette.add("Cheese");
        ingrediensForRaclette.add("Potatos");

        Meal raclette = new Meal(repository.getNewId(), EnumMealType.STARTER, ingrediensForRaclette);
        menuOneMealsRestaurantTwo.add(raclette);


        ArrayList<Meal> menuTwoRestaurantTwo = new ArrayList<>();
        menuTwoMealsRestaurantOne.add(pancakes);

        ArrayList<String> ingrediensForSpaghettiCarbonara = new ArrayList<>();
        ingrediensForSpaghettiCarbonara.add("Spaghetti");
        ingrediensForSpaghettiCarbonara.add("Carbonara sauce");

        Meal spaghettiCarbonara = new Meal(repository.getNewId(), EnumMealType.MAIN_COURSE, ingrediensForSpaghettiCarbonara);
        menuTwoRestaurantTwo.add(spaghettiCarbonara);

        Menu menuTwo = new Menu(menuTwoMealsRestaurantOne, 12.30f);
        menusRestaurantOne.add(menuTwo);

        Restaurant restaurantTwo = new Restaurant(repository.getNewId(), "Not local restaurant" , employeesRestaurantTwo,  menusRestaurantTwo, 3);

        restaurantTwo.setEmployees(employeesRestaurantTwo);
        restaurantTwo.setMenus(menusRestaurantTwo);
        repository.add(restaurantTwo);

        HashMap<Integer, Restaurant> restaurants = repository.getRestaurants();

        for (Restaurant restaurant : restaurants.values()) {
            System.out.println("Restaurant id : " + restaurant.getId());
            System.out.println("Restaurant name : " + restaurant.getName());
            System.out.println("Stars : " + restaurant.getStars());

            System.out.println();
            System.out.println("Employees : ");
            System.out.println();

            ArrayList<Employee> employees = restaurant.getEmployees();
            for (Employee employee: employees) {
                System.out.println("Name : " + employee.getTitle() + " " + employee.getFirstName() + " " + employee.getSecondName());
                System.out.println("Position : " + employee.getClass().getSimpleName());
                System.out.println();
            }

            System.out.println();
            System.out.println("Menus : ");
            System.out.println();

            ArrayList<Menu> menus = restaurant.getMenus();
            if(menus.isEmpty()){
                System.out.println("[No menus defined]");
            } else {
                for(Menu menu : menus) {
                    System.out.println("    Price : " + menu.getPrice());
                    System.out.println("        Meals : ");
                    ArrayList<Meal> meals = menu.getMeals();
                    for (Meal meal : meals) {
                        System.out.println("            Type : " + meal.getType().toString());
                        System.out.println("            Ingrediens : ");
                        ArrayList<String> ingrediens = meal.getIngredients();
                        if (ingrediens.isEmpty()) {
                            System.out.print("              [No ingredients defined]");
                        } else {
                            for (String ingredient : ingrediens) {
                                System.out.print(ingredient + ", ");
                            }
                        }
                        System.out.println();
                    }
                    System.out.println();
                }
            }
            System.out.println("----------------------------------------------------------------");
        }
	}
    // endregion
}