package hugo.io.tbz.M326.restaurant;

import java.util.HashMap;
import java.util.concurrent.atomic.AtomicLong;

public class Repository {

	// region Properties
	private AtomicLong sequencer = new AtomicLong();
	private HashMap<Integer, Restaurant> restaurants = new HashMap<>();
	// endregion

    // region Getters / Setters
	public HashMap<Integer, Restaurant>  getRestaurants() {
		return this.restaurants;
	}
	public void setRestaurants(HashMap<Integer, Restaurant>  restaurants) {
		this.restaurants = restaurants;
	}
	// endregion

    // region Methods
	/**
	 * 
	 * @param restaurant
	 */
	public void add(Restaurant restaurant) {
		HashMap<Integer, Restaurant> restaurantHashMap = getRestaurants();
		restaurantHashMap.put(restaurant.getId(), restaurant);
		setRestaurants(restaurantHashMap);
	}

    public Integer getNewId(){
        sequencer.addAndGet(1);
        return sequencer.intValue();
    }
    // endregion
}