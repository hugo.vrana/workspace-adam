package hugo.io.tbz.M326.restaurant;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class Waiter extends Employee {

    // region Properties
	private ArrayList<Meal> mealsToBring = new ArrayList<>();
	// endregion

    // region Getters / Setters
    public ArrayList<Meal> getMealsToBring() {
        return mealsToBring;
    }

    public void setMealsToBring(ArrayList<Meal> mealsToBring) {
        this.mealsToBring = mealsToBring;
    }
    // endregion

    // region Constructors
	public Waiter() { }

	/**
	 * 
	 * @param id
	 * @param title
	 * @param firstName
	 * @param secondName
	 * @param birthday
	 * @param workdays
	 * @param workingHours
	 */
	public Waiter(int id, String title, String firstName, String secondName, LocalDateTime birthday, ArrayList<DayOfWeek> workdays, ArrayList<String> workingHours) {
        super(id, title, firstName, secondName, birthday, workdays, workingHours, false);
	}

	/**
	 * 
	 * @param id
	 * @param title
	 * @param firstName
	 * @param secondName
	 * @param birthday
	 * @param workdays
	 * @param workingHours
	 * @param mealsToBring
	 */
	public Waiter(int id, String title, String firstName, String secondName, LocalDateTime birthday, ArrayList<DayOfWeek> workdays, ArrayList<String> workingHours, ArrayList<Meal> mealsToBring) {
        super(id, title, firstName, secondName, birthday, workdays, workingHours, false);
        this.mealsToBring = mealsToBring;
	}

	/**
	 * 
	 * @param employee
	 */
	public Waiter(Employee employee) {
        super(employee.getId(), employee.getTitle(), employee.getFirstName(), employee.getSecondName(), employee.getBirthday(), employee.getWorkdays(), employee.getWorkingHours(), employee.getWorking());
	}

	/**
	 * 
	 * @param employee
	 * @param mealsToBring
	 */
	public Waiter(Employee employee, ArrayList<Meal> mealsToBring) {
        super(employee.getId(), employee.getTitle(), employee.getFirstName(), employee.getSecondName(), employee.getBirthday(), employee.getWorkdays(), employee.getWorkingHours(), employee.getWorking());
        this.mealsToBring = mealsToBring;
	}
	// endregion

    // region Methods ! Not Implemented !
	/**
	 * 
	 * @param cook
	 * @param meal
	 */
	public void order(Cook cook, Meal meal) {
		// TODO - implement Waiter.order
		throw new UnsupportedOperationException();
	}

	/**
	 * 
	 * @param cook
	 * @param menu
	 */
	public void order(Cook cook, Menu menu) {
		// TODO - implement Waiter.order
		throw new UnsupportedOperationException();
	}

	public void orderAllMeals() {
		// TODO - implement Waiter.orderAllMeals
		throw new UnsupportedOperationException();
	}

	/**
	 * 
	 * @param cook
	 */
	public void orderAllMeals(Cook cook) {
		// TODO - implement Waiter.orderAllMeals
		throw new UnsupportedOperationException();
	}

	/**
	 * 
	 * @param meal
	 */
	public void orderMeal(Meal meal) {
		// TODO - implement Waiter.orderMeal
		throw new UnsupportedOperationException();
	}

	/**
	 * 
	 * @param meal
	 * @param cook
	 */
	public void orderMeal(Meal meal, Cook cook) {
		// TODO - implement Waiter.orderMeal
		throw new UnsupportedOperationException();
	}

	public void addMealToOrder(Meal meal){
		// TODO - implement Waiter.addMealToOrder
		throw new UnsupportedOperationException();
	}
	// endregion
}