package hugo.io.tbz.M326.restaurant;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class Cook extends Employee {

    //region Properties
    private ArrayList<Meal> mealsToCook;
    //endregion

    //region Constructors

    public Cook() {}

	/**
	 * @param id
	 * @param title
	 * @param firstName
	 * @param secondName
	 * @param birthday
	 * @param wordays
	 * @param workingHours
	 */
	public Cook(int id, String title, String firstName, String secondName, LocalDateTime birthday, ArrayList<DayOfWeek> wordays, ArrayList<String> workingHours) {
		super(id, title, firstName, secondName, birthday, wordays, workingHours, false);
	}

	/**
	 * 
	 * @param id
	 * @param title
	 * @param firstName
	 * @param secondName
	 * @param birthday
	 * @param wordays
	 * @param workingHours
	 * @param mealsToCook
	 */
	public Cook(int id, String title, String firstName, String secondName, LocalDateTime birthday, ArrayList<DayOfWeek> wordays, ArrayList<String> workingHours, ArrayList<Meal> mealsToCook, Boolean working) {
		super(id, title, firstName, secondName, birthday, wordays, workingHours, working);
		this.mealsToCook = mealsToCook;
	}

	/**
	 * 
	 * @param employee
	 */
	public Cook(Employee employee) {
		super(employee.getId(), employee.getTitle(), employee.getFirstName(), employee.getSecondName(), employee.getBirthday(), employee.getWorkdays(), employee.getWorkingHours(), employee.getWorking());
	}

	/**
	 * 
	 * @param employee
	 * @param mealsToCook
	 */
	public Cook(Employee employee, ArrayList<Meal> mealsToCook) {
		super(employee.getId(), employee.getTitle(), employee.getFirstName(), employee.getSecondName(), employee.getBirthday(), employee.getWorkdays(), employee.getWorkingHours(), employee.getWorking());
		this.mealsToCook = mealsToCook;
	}
    //endregion

    // region Methods
	/**
	 * 
	 * @param meal
	 */
	public void cook(Meal meal) {
		if(mealsToCook.contains(meal)){
			meal.setCooked(true);
			mealsToCook.remove(meal);
		}
	}

	/**
	 * 
	 * @param menu
	 */
	public void cook(Menu menu) {
		for (Meal meal: menu.getMeals()) {
			meal.setCooked(true);
            if(mealsToCook.contains(meal)) {
                mealsToCook.remove(meal);
            }
		}
	}

	public void cookAll() {
		for (Meal meal: mealsToCook) {
			meal.setCooked(true);
			mealsToCook.remove(meal);
		}
	}
    // endregion
}