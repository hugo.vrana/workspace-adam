package hugo.io.tbz.M326.restaurant;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.util.ArrayList;

public abstract class Employee extends Person {

    // region Properties
	private ArrayList<DayOfWeek> workdays;
	private ArrayList<String> workingHours;
    private Boolean working;
    // endregion

    // region Getters / Setters

    public Boolean getWorking() {
        return working;
    }

    public void setWorking(Boolean working) {
        this.working = working;
    }

    public ArrayList<DayOfWeek> getWorkdays() {
        return workdays;
    }

    public void setWorkdays(ArrayList<DayOfWeek> workdays) {
        this.workdays = workdays;
    }

    public ArrayList<String> getWorkingHours() {
        return workingHours;
    }

    public void setWorkingHours(ArrayList<String> workingHours) {
        this.workingHours = workingHours;
    }

    // endregion

    // region Constructors
	public Employee(){}

    public Employee(ArrayList<DayOfWeek> workdays, ArrayList<String> workingHours, Boolean working) {
        this.workdays = workdays;
        this.workingHours = workingHours;
        this.working = working;
    }

    public Employee(int id, String title, String firstName, String secondName, LocalDateTime birthday, ArrayList<DayOfWeek> workdays, ArrayList<String> workingHours, Boolean working) {
        super(id, title, firstName, secondName, birthday);
        this.workdays = workdays;
        this.workingHours = workingHours;
        this.working = working;
    }

    public Employee(int id, String firtstName, String secondName, ArrayList<DayOfWeek> workdays, ArrayList<String> workingHours, Boolean working) {
        super(id, firtstName, secondName);
        this.workdays = workdays;
        this.workingHours = workingHours;
        this.working = working;
    }
    // endregion

    // region Methods
    public void startWorking() {
        working = true;
    }

    public void stopWorking(){
        working = false;
    }
    // endregion
}