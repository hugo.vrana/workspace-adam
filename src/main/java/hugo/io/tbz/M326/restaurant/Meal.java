package hugo.io.tbz.M326.restaurant;

import java.time.LocalTime;
import java.util.ArrayList;

public class Meal {

    // region Properties
    private Integer id;
	private EnumMealType type;
	private ArrayList<String> ingredients;
	private LocalTime preparationTime;
    private Boolean cooked;
    // endregion

    // region Getters / Setters
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public EnumMealType getType() {
        if(type != null) {
            return type;
        }else{
            return EnumMealType.NOT_DEFINED;
        }
    }

    public void setType(EnumMealType type) {
        this.type = type;
    }

    public ArrayList<String> getIngredients() {
        if(ingredients != null) {
            return ingredients;
        }else{
            return new ArrayList<>();
        }
    }

    public void setIngredients(ArrayList<String> ingredients) {
        this.ingredients = ingredients;
    }

    public LocalTime getPreparationTime() {
        return preparationTime;
    }

    public void setPreparationTime(LocalTime preparationTime) {
        this.preparationTime = preparationTime;
    }

    public Boolean getCooked() {
        return cooked;
    }

    public void setCooked(Boolean cooked) {
        this.cooked = cooked;
    }

    // endregion

    // region Constructors
	public Meal() { }

	/**
	 * @param type
	 * @param ingredients
	 * @param preparationTime
	 */
	public Meal(Integer id, EnumMealType type, ArrayList<String> ingredients, LocalTime preparationTime) {
		this.id = id;
	    this.type = type;
		this.ingredients = ingredients;
		this.preparationTime = preparationTime;
		this.cooked = false;
	}

	/**
	 * @param type
	 * @param ingredients
	 */
	public Meal(Integer id, EnumMealType type, ArrayList<String> ingredients) {
		this.id = id;
	    this.type = type;
		this.ingredients = ingredients;
		this.preparationTime = LocalTime.of(0, 0);
		this.cooked = false;
	}
	// endregion
}