package hugo.io.tbz.M326.restaurant;

import java.util.ArrayList;

public class Menu {

    // region Properties
	private ArrayList<Meal> meals;
	private Float price;
    // endregion

    // region Getters / Setters
	public ArrayList<Meal> getMeals() {
		return meals;
	}

	public void setMeals(ArrayList<Meal> meals) {
		this.meals = meals;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}
    // endregion

    // region Constructors
	public Menu() { }

	/**
	 * 
	 * @param meals
	 * @param price
	 */
	public Menu(ArrayList<Meal> meals, Float price) {
		this.meals = meals;
		this.price = price;
	}
	// endregion
}