package hugo.io.tbz.M326.restaurant;

import java.time.LocalDateTime;

public class Person {

    // region Properties
    private int id;
    private String title = "";
    private String firstName = "";
    private String secondName = "";
    private LocalDateTime birthday = LocalDateTime.of(0,1,1,0, 0);
    // endregion

    // region Getters / Setters
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
            return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return this.secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public LocalDateTime getBirthday() {
        return this.birthday;
    }

    public void setBirthday(LocalDateTime birthday) {
        this.birthday = birthday;
    }

    // endregion

    // region Constructors
    public Person() {}

    /**
     *
     * @param id
     * @param title
     * @param firstName
     * @param secondName
     * @param birthday
     */
    public Person(int id, String title, String firstName, String secondName, LocalDateTime birthday) {
        this.id = id;
        this.title = title;
        this.firstName = firstName;
        this.secondName = secondName;
        this.birthday = birthday;
    }

    /**
     *
     * @param id
     * @param firtstName
     * @param secondName
     */
    public Person(int id, String firtstName, String secondName) {
        this.id = id;
        this.firstName = firtstName;
        this.secondName = secondName;
    }
    // endregion
}
