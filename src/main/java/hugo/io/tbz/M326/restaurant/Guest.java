package hugo.io.tbz.M326.restaurant;

import java.time.LocalDateTime;

public class Guest extends Person {

    // region Properties
	private Menu orderedMenu;
	private Boolean payed;
    private Boolean served;
    // endregion

    // region Getters / Setters
	public Menu getOrderedMenu() {
		return this.orderedMenu;
	}

	public void setOrderedMenu(Menu orderedMenu) {
		this.orderedMenu = orderedMenu;
	}

	public Boolean getPayed() {
		return this.payed;
	}

	public void setPayed(Boolean payed) {
		this.payed = payed;
	}

    public Boolean getServed() {
        return served;
    }

    public void setServed(Boolean served) {
        this.served = served;
    }

    // endregion

    //region Constructors
	public Guest() { }

	/**
	 * 
	 * @param orderedMenu
	 * @param payed
	 * @param person
	 */
	public Guest(Menu orderedMenu, Boolean payed, Person person) {
		super(person.getId(), person.getTitle(), person.getFirstName(), person.getSecondName(), person.getBirthday());
		this.orderedMenu = orderedMenu;
		this.payed = payed;
	}

	/**
	 * 
	 * @param orderedMenu
	 * @param payed
	 * @param id
	 * @param title
	 * @param firstName
	 * @param secondName
	 * @param birthday
	 */
	public Guest(Menu orderedMenu, Boolean payed, int id, String title, String firstName, String secondName, LocalDateTime birthday) {
		super(id, title, firstName, secondName, birthday);
		this.orderedMenu = orderedMenu;
		this.payed = payed;
	}

	/**
	 * 
	 * @param orderedMenu
	 * @param payed
	 * @param id
	 * @param firstName
	 * @param secondName
	 */
	public Guest(Menu orderedMenu, Boolean payed, int id, String firstName, String secondName) {
		super(id, firstName, secondName);
		this.orderedMenu = orderedMenu;
		this.payed = payed;
	}

    // endregion

    // region Methods

	/**
	 * 
	 * @param waiter
	 * @param menu
	 */
	public void order(Waiter waiter, Menu menu) {
		for (Meal meal: menu.getMeals()) {
			waiter.addMealToOrder(meal);
		}
	}

	/**
	 * 
	 * @param waiter
	 * @param meal
	 */
	public void order(Waiter waiter, Meal meal) {
        waiter.addMealToOrder(meal);
	}

	// endregion
}