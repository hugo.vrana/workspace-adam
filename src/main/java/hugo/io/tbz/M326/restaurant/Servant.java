package hugo.io.tbz.M326.restaurant;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class Servant extends Employee {

	// region Properties
	private ArrayList<Guest> guestsToServe = new ArrayList<>();
	// endregion

	// region Getters / Setters
	public ArrayList<Guest> getGuestsToServe() {
		return guestsToServe;
	}

	public void setGuestsToServe(ArrayList<Guest> guestsToServe) {
		this.guestsToServe = guestsToServe;
	}
	// endregion

	// region Constructors
	public Servant() { }

	/**
	 * 
	 * @param id
	 * @param title
	 * @param firstName
	 * @param secondName
	 * @param birthday
	 * @param wordays
	 * @param workingHours
	 */
	public Servant(int id, String title, String firstName, String secondName, LocalDateTime birthday, ArrayList<DayOfWeek> wordays, ArrayList<String> workingHours) {
		super(id, title, firstName, secondName, birthday, wordays, workingHours, false);
	}

	/**
	 * 
	 * @param id
	 * @param title
	 * @param firstName
	 * @param secondName
	 * @param birthday
	 * @param wordays
	 * @param workingHours
	 * @param guestToServe
	 */
	public Servant(int id, String title, String firstName, String secondName, LocalDateTime birthday, ArrayList<DayOfWeek> wordays, ArrayList<String> workingHours, ArrayList<Guest> guestToServe) {
        super(id, title, firstName, secondName, birthday, wordays, workingHours, false);
        this.guestsToServe = guestToServe;
	}

	/**
	 * 
	 * @param employee
	 */
	public Servant(Employee employee) {
         super(employee.getId(), employee.getTitle(), employee.getFirstName(), employee.getSecondName(), employee.getBirthday(), employee.getWorkdays(), employee.getWorkingHours(), employee.getWorking());
	}

	/**
	 * 
	 * @param employee
	 * @param guestsToServe
	 */
	public Servant(Employee employee, ArrayList<Guest> guestsToServe) {
       super(employee.getId(), employee.getTitle(), employee.getFirstName(), employee.getSecondName(), employee.getBirthday(), employee.getWorkdays(), employee.getWorkingHours(), employee.getWorking());
        this.guestsToServe = guestsToServe;
	}

	// endregion

	// region Methods
	/**
	 * 
	 * @param guest
	 */
	public Guest serve(Guest guest) {
       guest.setServed(true);
       return guest;
	}

	public ArrayList<Guest> serveAll() {
        for (Guest guest : guestsToServe) {
            serve(guest);
        }
        return guestsToServe;
	}
	// endregion
}