package hugo.io.tbz.M326.restaurant;

enum EnumMealType {
	SOUP,
	DESSERT,
	STARTER,
	MAIN_COURSE,
	NOT_DEFINED
}