package hugo.io.tbz.M326.kinobuchsystem;


import java.time.LocalDateTime;

public class Vorstellung {

	private Kinosaal kinosaal;
	private Film film;
	private LocalDateTime time;

	public Kinosaal getKinosaal() {
		return this.kinosaal;
	}

	public void setKinosaal(Kinosaal kinosaal) {
		this.kinosaal = kinosaal;
	}

	public Film getFilm() {
		return this.film;
	}

	public void setFilm(Film film) {
		this.film = film;
	}

	public LocalDateTime getTime() {
		return this.time;
	}

	public void setTime(LocalDateTime time) {
		this.time = time;
	}

	public Vorstellung() {
		// TODO - implement Vorstellung.Vorstellung
		throw new UnsupportedOperationException();
	}

	/**
	 * 
	 * @param kinosaal
	 * @param film
	 * @param time
	 */
	public Vorstellung(Kinosaal kinosaal, Film film, LocalDateTime time) {
		// TODO - implement Vorstellung.Vorstellung
		throw new UnsupportedOperationException();
	}

}