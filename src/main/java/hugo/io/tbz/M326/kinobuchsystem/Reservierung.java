package hugo.io.tbz.M326.kinobuchsystem;

public class Reservierung {

	private Platz platz;
	private Besucher besucher;
	private Vorstellung vorstellung;

	public Platz getPlatz() {
		return this.platz;
	}

	public void setPlatz(Platz platz) {
		this.platz = platz;
	}

	public Besucher getBesucher() {
		return this.besucher;
	}

	public void setBesucher(Besucher besucher) {
		this.besucher = besucher;
	}

	public Vorstellung getVorstellung() {
		return this.vorstellung;
	}

	public void setVorstellung(Vorstellung vorstellung) {
		this.vorstellung = vorstellung;
	}

	/**
	 * 
	 * @param id
	 */
	public Vorstellung getVorstellung(int id) {
		return this.vorstellung;
	}

	public Reservierung() {
		// TODO - implement Reservierung.Reservierung
		throw new UnsupportedOperationException();
	}

	/**
	 * 
	 * @param platz
	 * @param besucher
	 * @param vorstellung
	 */
	public Reservierung(Platz platz, Besucher besucher, Vorstellung vorstellung) {
		// TODO - implement Reservierung.Reservierung
		throw new UnsupportedOperationException();
	}

}