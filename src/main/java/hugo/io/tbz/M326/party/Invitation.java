package hugo.io.tbz.M326.party;

public class Invitation {
    private Integer id;
    private Person guest;
	private Boolean accepted;
    private Party party;

    public Integer getId() {return id;}

    public void setId(Integer id) {this.id = id;}

    public Person getGuest() {return guest;}

    public void setGuest(Person guest) {this.guest = guest;}

    public Boolean getAccepted() {return accepted;}

    public void setAccepted(Boolean accepted) {this.accepted = accepted;}

    public Party getParty() {return party;}

    public void setParty(Party party) { this.party = party; }

    public Invitation() {}

	/**
	 * 
	 * @param guest
	 */
	public Invitation(Integer id, Person guest, Party party) {
		this.id = id;
	    this.guest = guest;
	    this.party = party;
	    this.accepted = false;
	}
}