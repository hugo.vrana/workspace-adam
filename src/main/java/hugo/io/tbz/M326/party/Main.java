package hugo.io.tbz.M326.party;

import hugo.io.other.cosoleUtility.myconsole.MyConsole;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;


public class Main {

    public static Repository repository = new Repository();

    public static void main(String[] args) {
        Location partyLocation = new Location(repository.getNewId(), "Partystreet", 23, "New York", 304);
        repository.add(partyLocation);

        Party party = new Party(repository.getNewId(), partyLocation, LocalDateTime.of(2019, 12, 5, 14, 30));
        repository.add(party);

        Location guests1Location = new Location(repository.getNewId(), "Masterstreet", 15, "London", 7892);
        repository.add(guests1Location);

        Location guest2Location = new Location(repository.getNewId(), "Imaginarystreet", 32, "Zurich", 123);

        Person guest1 = new Person(repository.getNewId(), "Mr", "Peter", "Muster", LocalDateTime.now(), guests1Location);
        repository.add(guest1);

        Person guest2 = new Person(repository.getNewId(), "Mrs", "Viviana" , "Müller", LocalDateTime.now(), guest2Location);
        repository.add(guest2);

        Invitation invitationForGuest1 = new Invitation(repository.getNewId(), guest1, party);
        repository.add(invitationForGuest1);

        Invitation invitationForGuest2 = new Invitation(repository.getNewId(), guest2, party);
        invitationForGuest2.setAccepted(true);
        repository.add(invitationForGuest2);

        printValues();
    }

    private static void printValues(){
        HashMap<Integer, Party> parties = repository.getParties();
        HashMap<Integer, Invitation> invitations = repository.getInvitations();
        MyConsole console = new MyConsole();
        console.printLine("Parties");
        console.printLine();

        for (Party p: parties.values()){
            console.printLine("Id : " + p.getId());
            console.printLine("Location id: " + p.getLocation().getId());
            console.printLine("Street : " + p.getLocation().getStreet());
            console.printLine("Street number : " + p.getLocation().getNumber());
            console.printLine("City : " + p.getLocation().getCity());
            console.printLine("PLZ :" + p.getLocation().getPlz());
            console.printLine("Date : " + p.getTime().format(DateTimeFormatter.ofPattern("dd.MM.YYYY hh:mm:ss")));
            console.printLine();
            console.printLine("Guests");
            console.printLine();

            for (Invitation i: invitations.values()){
                if (i.getParty().getId() == p.getId()){
                    Person guest = i.getGuest();
                    console.printLine("Invitaton id : " + i.getId());
                    console.printLine("Name : " + guest.getTitle() + " " + guest.getFirstName() + " " + guest.getSecondName());
                    console.printLine("Birthday : " + guest.getBirthday().format(DateTimeFormatter.ofPattern("dd.MM.YYYY hh:mm:ss")));
                    console.printLine();
                }
            }
        }
    }
}
