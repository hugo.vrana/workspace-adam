package hugo.io.tbz.M326.party;

import java.util.HashMap;
import java.util.concurrent.atomic.AtomicLong;

public class Repository {

    private final AtomicLong sequencer = new AtomicLong();
    private HashMap<Integer, Person> persons;
	private HashMap<Integer, Party> parties;
	private HashMap<Integer, Location> locations;
	private HashMap<Integer, Invitation> invitations;

    public HashMap<Integer, Person> getPersons() {
        return persons;
    }

    public void setPersons(HashMap<Integer, Person> persons) {
        this.persons = persons;
    }

    public HashMap<Integer, Party> getParties() {
        return parties;
    }

    public void setParties(HashMap<Integer, Party> parties) {
        this.parties = parties;
    }

    public HashMap<Integer, Location> getLocations() {
        return locations;
    }

    public void setLocations(HashMap<Integer, Location> locations) {
        this.locations = locations;
    }

    public HashMap<Integer, Invitation> getInvitations() {
        return invitations;
    }

    public void setInvitations(HashMap<Integer, Invitation> invitations) {
        this.invitations = invitations;
    }

	public Repository() {
		persons = new HashMap<>();
		parties = new HashMap<>();
        locations = new HashMap<>();
        invitations = new HashMap<>();
	}

	/**
	 * 
	 * @param person
	 */
	public void add(Person person) {
		HashMap<Integer, Person> persons = getPersons();
		persons.put(person.getId(), person);
		setPersons(persons);
	}

	/**
	 * 
	 * @param party
	 */
	public void add(Party party) {
        HashMap<Integer, Party> parties = getParties();
        parties.put(party.getId(), party);
        setParties(parties);
	}

	/**
	 * 
	 * @param location
	 */
	public void add(Location location) {
        HashMap<Integer, Location> locations = getLocations();
        locations.put(location.getId(), location);
        setLocations(locations);
	}

	/**
	 * 
	 * @param invitation
	 */
	public void add(Invitation invitation) {
		HashMap<Integer, Invitation> invitations = getInvitations();
		invitations.put(invitation.getId(), invitation);
		setInvitations(invitations);
	}

	public Integer getNewId(){
	    sequencer.addAndGet(1);
        return sequencer.intValue();
    }
}