package hugo.io.tbz.M326.party;

import java.time.LocalDateTime;

public class Person {

	private int id;
	private String title;
	private String firstName;
	private String secondName;
	private LocalDateTime birthday;
	private Location location;

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSecondName() {
		return this.secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	public LocalDateTime getBirthday() {
		return this.birthday;
	}

	public void setBirthday(LocalDateTime birthday) {
		this.birthday = birthday;
	}

	public Location getLocation() {
		return this.location;
	}

	public void setLocation(Location location){ this.location = location; }

	public Person() {}

	/**
	 * 
	 * @param id
	 * @param title
	 * @param firstName
	 * @param secondName
	 * @param birthday
	 * @param location
	 */
	public Person(int id, String title, String firstName, String secondName, LocalDateTime birthday, Location location) {
		this.id = id;
		this.title = title;
		this.firstName = firstName;
		this.secondName = secondName;
		this.birthday = birthday;
		this.location = location;
	}

	/**
	 * 
	 * @param id
	 * @param firtstName
	 * @param secondName
	 * @param location
	 */
	public Person(int id, String firtstName, String secondName, Location location) {
		this.id = id;
		this.firstName = firtstName;
		this.secondName = secondName;
		this.location = location;
	}
}