package hugo.io.tbz.M326.party;

import java.time.LocalDateTime;

public class Host extends Person {

	private Party party;

	public Party getParty() {
		return this.party;
	}

	public void setParty(Party party) {
		this.party = party;
	}

	public Host() {}

    public Host(int id, String title, String firstName, String secondName, LocalDateTime birthday, Location location, Party party) {
        super(id, title, firstName, secondName, birthday, location);
        this.party = party;
    }

    public Host(int id, String firtstName, String secondName, Location location, Party party) {
        super(id, firtstName, secondName, location);
        this.party = party;
    }
}