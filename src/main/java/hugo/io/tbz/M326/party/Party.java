package hugo.io.tbz.M326.party;

import java.time.LocalDateTime;

public class Party {

	private int id;
	private Location location;
	private LocalDateTime time;

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Location getLocation() {
		return this.location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public LocalDateTime getTime() {
		return this.time;
	}

	public void setTime(LocalDateTime time) {
		this.time = time;
	}

	public Party() {}

	/**
	 * @param id
	 * @param location
	 * @param time
	 */
	public Party(int id, Location location, LocalDateTime time) {
		this.id = id;
		this.location = location;
		this.time = time;
	}

}