package hugo.io.tbz.M326.party;

public class Location {

	private Integer id;
	private String street;
	private Integer number;
	private String city;
	private Integer plz;
	private Integer locatioId;

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStreet() {
		return this.street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public int getNumber() {
		return this.number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getPlz() {
		return this.plz;
	}

	public void setPlz(int plz) {
		this.plz = plz;
	}

	public int getLocatioId() {
		return this.locatioId;
	}

	public void setLocatioId(int locatioId) {
		this.locatioId = locatioId;
	}

	public Location() { }

    public Location(int id, String street, int number, String city, int plz) {
        this.id = id;
        this.street = street;
        this.number = number;
        this.city = city;
        this.plz = plz;
    }

	/**
	 * 
	 * @param id
	 * @param street
	 * @param number
	 * @param city
	 * @param plz
	 * @param locationId
	 */
	public Location(int id, String street, int number, String city, int plz, int locationId) {
		this.id = id;
		this.street = street;
		this.number = number;
		this.city = city;
		this.plz = plz;
		this.locatioId = locationId;
	}

}