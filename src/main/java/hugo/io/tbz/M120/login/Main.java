package hugo.io.tbz.M120.login;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;


public class Main extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        primaryStage.setTitle("Java FX Welcome");

        GridPane gridPane = new GridPane();
        gridPane.setAlignment(Pos.CENTER);
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(25, 25, 25,25));

        Text txtSceneTitle = new Text("Welcome");
        txtSceneTitle.setFont(Font.font("Verdana", FontWeight.NORMAL, 20));
        gridPane.add(txtSceneTitle, 0, 0,2, 1);

        Label lblUserName = new Label("User Name: ");
        lblUserName.setFont(Font.font("Verdana", FontWeight.NORMAL, 20));
        gridPane.add(lblUserName, 0, 1);

        TextField txtUserName = new TextField();
        gridPane.add(txtUserName, 1, 1);

        Label lblPassword = new Label("Password: ");
        lblPassword.setFont(Font.font("Verdana", FontWeight.NORMAL, 20));
        gridPane.add(lblPassword, 0, 2);

        PasswordField txtPassword = new PasswordField();
        gridPane.add(txtPassword, 1, 2);

        HBox hbLogin = new HBox(10);
        hbLogin.setAlignment(Pos.BOTTOM_RIGHT);

        final Text txtActionTarget = new Text();
        gridPane.add(txtActionTarget, 1, 6);

        Button btnLogin = new Button("Sign in");
        btnLogin.setOnAction(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent e){
                txtActionTarget.setFill(Color.FIREBRICK);
                txtActionTarget.setText("Sign in button pressed!");
            }
        });

        hbLogin.getChildren().add(btnLogin);

        gridPane.add(hbLogin, 1, 4);

        Scene scene = new Scene(gridPane, 300, 275);
        scene.getStylesheets().add(Main.class.getResource("hugo/io/tbz/M120/login/Login.css").toExternalForm());
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
