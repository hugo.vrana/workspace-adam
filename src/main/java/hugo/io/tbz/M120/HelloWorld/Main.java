package hugo.io.tbz.M120.HelloWorld;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Hello World!");

        Label label = new Label();
        label.setText("Hello World!");
        label.setAlignment(Pos.CENTER);
        label.setFont(Font.font("Arial", 50));
        label.setPrefSize(1000, 1000);

        AnchorPane root = new AnchorPane();
        root.getChildren().add(label);

        primaryStage.setScene(new Scene(root, 1000, 1000));
        primaryStage.show();
    }
}