package hugo.io.tbz.M226b.snakes.main;

import hugo.io.tbz.M226b.snakes.gameEngine.GameLoop;
import hugo.io.tbz.M226b.snakes.helpers.KeyboardListener;

import javax.swing.*;

public class MainWindow extends JFrame {
    /**
     * Simple JFrame as user interface
     */
    public MainWindow() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(1000, 600);
        setExtendedState(MAXIMIZED_BOTH);
        setTitle("Neural Net Snake Genetic Algorithm");
        KeyboardListener keyb = new KeyboardListener();
        addKeyListener(keyb);
        add(new GameLoop(keyb));
        setVisible(true);
    }

    /**
     * Main function of the whole simulation
     */
    public static void main(String[] args) {
        new MainWindow();
    }
}