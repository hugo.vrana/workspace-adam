package hugo.io.tbz.M226b.cars;

import hugo.io.other.cosoleUtility.myconsole.MyConsole;
import hugo.io.tbz.M226b.cars.model.Car;
import hugo.io.tbz.M226b.cars.model.CrashedCar;
import hugo.io.tbz.M226b.cars.model.UsedCar;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        List<Car> cars = new ArrayList<>();

        // creating cars
        cars.add(new CrashedCar("Opel Astra", 20000.90, CrashLevel.LOW));
        cars.add(new CrashedCar("Mitsubishi 124", 600.0, CrashLevel.TOTAL));
        cars.add(new UsedCar("Skoda Fabia", 19800.0, 20000));
        cars.add(new UsedCar("Jeep Renegate", 19800.0, 20000));

        // showing cars
        MyConsole console = new MyConsole();
        for (Car car : cars) {
            console.printLine(car.getModel() + ": " + car.getPrice());
        }
    }
}