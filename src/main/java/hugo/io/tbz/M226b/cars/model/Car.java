package hugo.io.tbz.M226b.cars.model;

public abstract class Car {

    private String model;
    private Double price;
    public Car(String model, Double price) {
        this.model = model;
        this.price = price;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Double getPrice() {
        return price;
    }
}
