package hugo.io.tbz.M226b.cars.model;

import hugo.io.tbz.M226b.cars.CrashLevel;

public class CrashedCar extends Car {

    CrashLevel damageLevel;

    public CrashedCar(String model, Double price, CrashLevel damageLevel) {
        super(model, price);
        this.damageLevel = damageLevel;
    }

    public CrashLevel getDamageLevel() {
        return damageLevel;
    }

    public void setDamageLevel(CrashLevel damageLevel) {
        this.damageLevel = damageLevel;
    }

    @Override
    public Double getPrice() {
        Double price = super.getPrice();
        switch (getDamageLevel()) {
            case LOW:
                price = price - (10 * price / 100);
                break;
            case MEDIUM:
                price = price - (50 * price / 100);
                break;
            case HIGH:
                price = price - (90 * price / 100);
                break;
            case TOTAL:
                price = Double.valueOf(0);
                break;
        }
        return price;
    }
}
