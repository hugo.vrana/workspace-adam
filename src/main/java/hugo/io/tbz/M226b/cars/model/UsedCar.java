package hugo.io.tbz.M226b.cars.model;

public class UsedCar extends Car {
    private Integer milage;

    public UsedCar(String model, Double price, Integer milage) {
        super(model, price);
        this.milage = milage;
    }

    public Integer getMilage() {
        return milage;
    }

    @Override
    public Double getPrice() {
        if (milage < 10000) {
            return super.getPrice();
        } else {
            double price = super.getPrice();
            int counter = 0;
            int mileage = getMilage();
            while (mileage > 10000) {
                counter += 1;
                mileage -= 10000;
            }
            price = price - ((price / 100 * 5) * counter);
            return price;
        }
    }
}
