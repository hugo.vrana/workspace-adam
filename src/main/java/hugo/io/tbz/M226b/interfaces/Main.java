package hugo.io.tbz.M226b.interfaces;

import hugo.io.tbz.M226b.interfaces.model.Car;
import hugo.io.tbz.M226b.interfaces.model.ICar;
import hugo.io.tbz.M226b.interfaces.model.Truck;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList<ICar> cars = new ArrayList<>();
        ICar car = new Car(4, 180, 60, false, 5);
        cars.add(car);
        ICar truck = new Truck(12, 120, 0, true);
        cars.add(truck);

        car.drive(60);
        car.stop();
        car.openDoors();
        car.closeDoors();
    }
}
