package hugo.io.tbz.M226b.interfaces.model;

public class Truck extends Vehicle implements ICar {

    public Truck(int wheelCount, int maxSpeed, int currentSpeed, boolean doorsOpened) {
        super(wheelCount, maxSpeed, currentSpeed, doorsOpened);
    }

    @Override
    public void drive(int currentSpeed) {
        if (!super.doorsOpened) {
            this.currentSpeed = currentSpeed;
        } else {
            System.out.println("You need to close the door first");
        }
    }

    @Override
    public void stop() {
        this.currentSpeed = 0;
    }

    @Override
    public void openDoors() {
        if (!super.doorsOpened) {
            super.doorsOpened = true;
        } else {
            System.out.println("Door is already opened");
        }
    }

    @Override
    public void closeDoors() {
        if (super.doorsOpened) {
            super.doorsOpened = false;
        } else {
            System.out.println("Door is already closed");
        }
    }

    @Override
    public void print() {

    }
}
