package hugo.io.tbz.M226b.interfaces.model;

public interface ICar {
    // Sets cars speed in km/h
    void drive(int currentSpeed);

    // Sets cars speed in km/h to 0
    void stop();

    // Sets cars door state to open
    void openDoors();

    // Sets cars door state to closed
    void closeDoors();

    // prints all values in console
    void print();
}
