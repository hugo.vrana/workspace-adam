package hugo.io.tbz.M226b.interfaces.model;

abstract class Vehicle {
    public int wheelCount;
    public int maxSpeed;
    public int currentSpeed;
    public boolean doorsOpened;

    protected Vehicle(int wheelCount, int maxSpeed, int currentSpeed, boolean doorsOpened) {
        this.wheelCount = wheelCount;
        this.maxSpeed = maxSpeed;
        this.currentSpeed = currentSpeed;
        this.doorsOpened = doorsOpened;
    }
}
