package hugo.io.azo.Ueb08aAufgB;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class MainArrayListForEach {
    public static void main(String[] args) {
        System.out.println("Bitte erste Zahl eingeben");
        Integer erstesZahl = input();

        System.out.println("Bitte zweite Zahl eingeben");
        Integer zweitesZahl = input();

        System.out.println("Bitte dritte Zahl eingeben");
        Integer drittesZahl = input();

        ArrayList<Integer> zahlen = new ArrayList<Integer>();
        zahlen.add(erstesZahl);
        zahlen.add(zweitesZahl);
        zahlen.add(drittesZahl);

        for (Integer zahl : zahlen) {
            System.out.print(zahl + " , ");
        }
    }

    public static Integer input() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String eingabe = "";

        try {
            eingabe = reader.readLine();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

        Integer iEingabe = null;

        try {
            iEingabe = Integer.parseInt(eingabe);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

        return iEingabe;
    }
}
