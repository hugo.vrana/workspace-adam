package hugo.io.azo.Ueb03bAufgB;

public class MyOperatorLogisch {

    public static void main(String[] args) {
        if ("A" == "A") // Gleich Operator mit string
        {
            System.out.println("A ist gleich A");
        }

        if (!("B" == "b")) // NOT Operator
        {
            System.out.println("B ist gleich b");
        }

        if ('C' == 'C') // Gleich Operator mit char
        {
            System.out.println("C ist gleich C");
        }

        int fuenf = 5; // Gleich Operator mit int
        if (5 == fuenf) {
            System.out.println("5 == fuenf");
        }

        if (fuenf != 6) // Nicht gleich Operator
        {
            System.out.println("5 ist nich gleich 6");
        }

        if (fuenf >= 4) {
            System.out.println("5 ist gr�sser gleich 4");
        }

        boolean frau = true;
        boolean weiblich = true;

        if (frau == weiblich) {
            System.out.println("Frau ist gleich weiblich");
        }
    }

}
