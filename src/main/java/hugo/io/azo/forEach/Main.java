package hugo.io.azo.forEach;

import java.time.LocalDate;

public class Main {

    public static void main(String args[]) {
        Auto auto = Auto.getInstance();
        auto.setBrand("Ferrari");
        auto.setDate(LocalDate.of(2015, 9, 23));

    }
}
