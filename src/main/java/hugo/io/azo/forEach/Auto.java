package hugo.io.azo.forEach;

import java.time.LocalDate;

public class Auto {

    private Integer id;
    private String brand;
    private String model;
    private Integer power;
    private LocalDate date;

    private Auto() {
    }

    public static Auto getInstance() {
        return new Auto();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getPower() {
        return power;
    }

    public void setPower(Integer power) {
        this.power = power;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }
}