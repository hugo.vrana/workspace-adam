package hugo.io.azo.Ueb03aAufgC;

public class MainRangeValue {
    public static void main(String[] Args) {
        System.out.println("Boolean");
        System.out.println(Boolean.FALSE);
        System.out.println(Boolean.TRUE);
        System.out.println();

        System.out.println("Byte");
        System.out.println(Byte.MIN_VALUE);
        System.out.println(Byte.MAX_VALUE);
        System.out.println();

        System.out.println("Short");
        System.out.println(Short.MIN_VALUE);
        System.out.println(Short.MAX_VALUE);
        System.out.println();

        System.out.println("Integer");
        System.out.println(Integer.MIN_VALUE);
        System.out.println(Integer.MAX_VALUE);
        System.out.println();

        System.out.println("Long");
        System.out.println(Long.MIN_VALUE);
        System.out.println(Long.MAX_VALUE);
        System.out.println();

        System.out.println("Float");
        System.out.println(Float.MIN_VALUE);
        System.out.println(Float.MAX_VALUE);
        System.out.println();

        System.out.println("Double");
        System.out.println(Double.MIN_VALUE);
        System.out.println(Double.MAX_VALUE);
        System.out.println();

        System.out.println("Character");
        System.out.println(Character.MIN_VALUE);
        System.out.println(Character.MAX_VALUE);
    }
}