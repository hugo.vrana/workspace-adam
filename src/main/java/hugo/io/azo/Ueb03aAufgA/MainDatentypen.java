package hugo.io.azo.Ueb03aAufgA;

public class MainDatentypen {
    public static void main(String[] Args) {
        boolean wahr = true;
        boolean unwahr = false;
        byte achtBitZahl = 127; // groesste Zahl byte - Zahl
        short sechzehnBitZahl = 32767; // groesste short - Zahl
        int _4ByteZahl = 214783647; // groesste int - Zahl
        long _8ByteZahl = 9999999999L; // groesste long - Zahl
        float _32BitZahl = 123.456f;
        double _64BitZahl = 456.789d;

        // Der kleinste Wert als Int
        int kleinsterInt = java.lang.Integer.MIN_VALUE;
        System.out.println("Der Kleinste int ist: " + kleinsterInt);

        // Was geschiet wenn der Zahlenbereich �berschritten bzw. unterschritten wird ?
        achtBitZahl += 1;
        System.out.println("127 Ueberschritten um 1 ergibt: " + achtBitZahl);
        System.out.println("-128 ist uebrigens die kleinse Zahl der Typs: byte");
        System.out.println(Float.BYTES);
    }

}
