package hugo.io.azo.standardObjects;

import java.util.Date;

public class Person {
    private String Name;
    private String Address;
    private String City;
    private String Email;
    private String HomePhone;
    private String WorkPhone;
    private java.util.Date Geburtsdatum;

    public Person(String name, String address, String city, String email, String homePhone, String workPhone, java.util.Date geburtsDatum) {

        this.Name = name;
        this.Address = address;
        this.City = city;
        this.Email = email;
        this.HomePhone = homePhone;
        this.WorkPhone = workPhone;
        this.Geburtsdatum = geburtsDatum;
    }


    public String getName() {
        return Name;
    }

    public void setName(String name) {
        this.Name = name;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        this.Address = address;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        this.City = city;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        this.Email = email;
    }

    public String getHomePhone() {
        return HomePhone;
    }

    public void setHomePhone(String homePhone) {
        this.HomePhone = homePhone;
    }

    public String getWorkPhone() {
        return WorkPhone;
    }

    public void setWorkPhone(String workPhone) {
        this.WorkPhone = workPhone;
    }

    public java.util.Date getGeburtsdatum() {
        return Geburtsdatum;
    }

    public void setGeburtsDatum(Date geburtsDatum) {
        this.Geburtsdatum = geburtsDatum;
    }
}