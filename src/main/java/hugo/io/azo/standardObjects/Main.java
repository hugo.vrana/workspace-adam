package hugo.io.azo.standardObjects;

import java.util.Date;

public class Main {
    public static void main(String[] args) {
        String Name = "Chuck Norris";
        String Adress = "Imaginationstrasse";
        String City = "Gothamcity";
        String Email = "gmail@chucknorris.com";
        String HomePhone = "1234567890";
        String Workphone = "0987654321";
        Date GeburtsDatum = new Date(1999 - 01 - 01);

        Person ChuckNorris = new Person(Name, Adress, City, Email, HomePhone, Workphone, GeburtsDatum);


        System.out.println("Name: " + ChuckNorris.getName());
        System.out.println("Strasse: " + ChuckNorris.getAddress());
        System.out.println("City: " + ChuckNorris.getCity());
        System.out.println("Email: " + ChuckNorris.getEmail());
        System.out.println("Home Phone: " + ChuckNorris.getHomePhone());
        System.out.println("Work Phone: " + ChuckNorris.getWorkPhone());
        System.out.println("Geburtsdatum: " + ChuckNorris.getGeburtsdatum());
    }

}
