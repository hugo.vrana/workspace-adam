package hugo.io.azo.Ueb06aAufgD;

public class mainDoWhileLoop {
    public static void main(String[] args) {
        int id = 97;
        do {
            char buchstabe = (char) id;
            System.out.print(buchstabe + " ");
            id++;
        }
        while (id < 123);

        System.out.println();

        id = 122;
        do {
            char buchstabe = (char) id;
            System.out.print(buchstabe + " ");
            id--;
        }
        while (id > 96);
    }
}