package hugo.io.azo.phoneBook;

import hugo.io.other.cosoleUtility.myconsole.MyConsole;

import java.util.HashMap;
import java.util.Map;


public class MyMap {

    HashMap<String, String> numbers = new HashMap<>();

    public HashMap<String, String> getNumbers() {
        return numbers;
    }

    public void setNumbers(HashMap<String, String> number) {
        this.numbers = number;
    }

    public void addNumbers() {
        MyConsole console = new MyConsole();
        for (int i = 0; i < 10; i++) {
            console.printLine("Name:");
            String name = console.readLineString();
            console.printLine("Number:");
            String number = console.readLineString();
            HashMap<String, String> numbers = getNumbers();
            numbers.put(name, number);
        }
        setNumbers(numbers);
    }

    public void showNumbers() {
        Map<String, String> map = getNumbers();
        map.forEach((k, v) -> System.out.println("Key: " + k + ", Value: " + v + ";"));
    }
}
