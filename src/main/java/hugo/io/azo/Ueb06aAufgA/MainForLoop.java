package hugo.io.azo.Ueb06aAufgA;

public class MainForLoop {
    public static void main(String[] args) {
        for (int id = 97; id < 123; id++) {
            char buchstabe = (char) id;
            System.out.print(buchstabe + " ");
        }

        System.out.println();

        for (int id = 122; id > 96; id--) {
            char buchstabe = (char) id;
            System.out.print(buchstabe + " ");
        }
    }
}