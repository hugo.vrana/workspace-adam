package hugo.io.azo.Ueb04aAufgA;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class MainConsoleReader {

    public static void main(String[] args) {
        System.out.print("Wie alt sind Sie? ");
        System.out.flush();

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        try {
            System.out.println("Sie sind: " + reader.readLine() + " Jahren alt.");
            return;
        } catch (Exception ex) {
            // Fehler augetreten, Meldung an Benutzer eingeben
            System.out.println(ex.getMessage());
        } finally {
            System.out.println("Diese Zeile word immer aufgerufen, obwohl oben ein Return ist.");
        }
    }

}
