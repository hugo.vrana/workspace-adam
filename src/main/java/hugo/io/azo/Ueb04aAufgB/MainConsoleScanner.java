package hugo.io.azo.Ueb04aAufgB;

import java.util.Scanner;

public class MainConsoleScanner {

    public static void main(String[] args) {
        System.out.print("Wie alt sind Sie?");
        System.out.flush();

        Scanner sc = new Scanner(System.in);
        int i = sc.nextInt();
        sc.close();

        System.out.println("Sie sind " + i + " Jahren alt.");

    }

}
