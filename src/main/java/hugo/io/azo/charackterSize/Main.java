package hugo.io.azo.charackterSize;

/**
 * Created by hvran on 10.03.2017.
 */
public class Main {
    public static void main(String[] args) {
        System.out.println("Char Bits: " + Character.SIZE);
        System.out.println("Integer Bits: " + Integer.SIZE);
        System.out.println("Float Bits: " + Float.SIZE);
        System.out.println("Double Bits: " + Double.SIZE);
        System.out.println("Long Bits: " + Long.SIZE);

    }

}