package hugo.io.azo.numberGuess;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by hugo on 05.05.17.
 */
public class Main {
    public static void main(String[] Args) {

        Double random = Math.random();
        random = random * 20;
        Integer rnd = random.intValue();
        Boolean guessed = Boolean.FALSE;
        Integer attempt = 0;

        System.out.println("Guess the number between 0 and 20");
        while (guessed == Boolean.FALSE) {
            Integer guess = input();
            attempt++;
            if (rnd == guess) {
                System.out.println("Your guess was right.");
                if (attempt > 1) {
                    System.out.println("You needed " + attempt + " attempts.");
                } else {
                    System.out.println("You needed " + attempt + " attempt.");
                }
                guessed = Boolean.TRUE;
            } else if (rnd < guess) {
                System.out.println("Too high");
            } else if (rnd > guess) {
                System.out.println("Too low");
            }
        }
    }

    public static Integer input() {
        System.out.print("Guess!");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            Integer input = Integer.parseInt(reader.readLine());
            return input;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return 0;
        }
    }
}