package hugo.io.azo.Ueb03aAufgE;

public class MyChars {
    public static void main(String[] Args) {
        char ausbildungs = 'a';
        char zentrum = 'z';
        char oberland = 'o';

        // Buchstaben ausgeben
        System.out.print(ausbildungs);
        System.out.print(zentrum);
        System.out.print(oberland);

        // Warum werden Zahlen anstatt Buchstaben ausgeben?
        System.out.println(ausbildungs + zentrum + oberland);
    }
}
