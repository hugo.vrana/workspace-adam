package hugo.io.azo.calc;

import hugo.io.tbz.M226a.cli.QuestionFlow;
import hugo.io.tbz.M226a.cli.questions.QuestionSelect;
import hugo.io.tbz.M226a.cli.questions.QuestionValueNumeric;

import java.io.IOException;

/**
 * Created by hugo on 17.04.17.
 */
public class Main {

    public static void main(String[] Args) throws IOException {

        QuestionFlow questionFlow = QuestionFlow.getInstance()
                .addQuestion(QuestionValueNumeric.getInstance()
                        .prompt("Insert first value")
                )
                .addQuestion(QuestionSelect.getInstance()
                        .addOptions(Operation.values())
                        .prompt("Choose operation")
                )
                .addQuestion(QuestionValueNumeric.getInstance()
                        .prompt("Insert second value")
                )
                .start();

        Double result = Calc.getInstance()
                .firstValue(Long.valueOf(questionFlow.getFlow().get(0).getAnswer()))
                .operation(questionFlow.getFlow().get(1).getAnswer())
                .secondValue(Long.valueOf(questionFlow.getFlow().get(2).getAnswer()))
                .calculate();

        System.out.println("Result is: " + result);
    }
}
