package hugo.io.azo.calc;

/**
 * Created by hugo on 17.04.17.
 */
public class Calc {
    private Long firstValue;
    private Long secondValue;
    private Operation operation;

    private Calc() {
        this.operation = Operation.PLUS;
    }

    public static Calc getInstance() {
        return new Calc();
    }

    public Calc firstValue(Long firstValue) {
        this.firstValue = firstValue;
        return this;
    }

    public Calc secondValue(Long secondValue) {
        this.secondValue = secondValue;
        return this;
    }

    public Calc operation(String operation) {
        for (Operation o : Operation.values()) {
            if (o.getCode().equals(operation)) {
                this.operation = o;
            }
        }
        return this;
    }

    public Double calculate() {
        Double result = Double.valueOf(0);
        switch (this.operation) {
            case PLUS:
                result = Double.valueOf(Math.addExact(this.firstValue, this.secondValue));
                break;

            case MINUS:
                result = Double.valueOf(Math.subtractExact(this.firstValue, this.secondValue));
                break;

            case MULTIPLY:
                result = Double.valueOf(Math.multiplyExact(this.firstValue, this.secondValue));
                break;

            case DIVIDE:
                result = Double.valueOf(this.firstValue / this.secondValue);
                break;

            default:
                result = Double.valueOf(0);
        }
        return result;
    }

}