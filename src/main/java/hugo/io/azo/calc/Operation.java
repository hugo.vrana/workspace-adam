package hugo.io.azo.calc;

import hugo.io.tbz.M226a.cli.questions.QuestionOptions;

/**
 * Created by hugo on 28.04.17.
 */
public enum Operation implements QuestionOptions {

    PLUS("+", "plus"),
    MINUS("-", "minus"),
    MULTIPLY("*", "multiply"),
    DIVIDE("/", "divide"),;

    private String code;
    private String name;

    Operation(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

}
