package hugo.io.azo.Ueb07aAufgA;

import java.io.IOException;
import java.util.Scanner;

public class MainCPlusPlus {

    public static void main(String[] args) throws IOException {
        Character input = Character.MIN_VALUE;
        while (input != 'b') {
            // Menu darstellen
            System.out.println();
            System.out.println("MENU");
            System.out.println("a: Start");
            System.out.println("c: Hallo Sagen");
            System.out.println("b: Beenden");
            System.out.println();
            System.out.println("Eingabe: ");

            // Einlesen von Input
            Scanner scan = new Scanner(System.in);
            String sEingabe = scan.nextLine();
            scan.close();

            switch (sEingabe) {
                case "a":
                    System.out.println("Sie haben a eingegeben");
                    break;
                case "b":
                    System.out.println("Es ist fertig.");
                    break;
                case "c":
                    System.out.println("Hallo.");
                    break;
                default:
                    System.out.println("Sie haben etwas anderes eingegeben.");
                    break;
            }
        }
    }
}