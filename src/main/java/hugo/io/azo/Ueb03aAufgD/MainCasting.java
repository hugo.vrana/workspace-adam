package hugo.io.azo.Ueb03aAufgD;

// Uebung03a, Aufgabe D
public class MainCasting {

    public static void main(String[] args) {
        // Implizit
        int jahrgang = 1999;
        double dJahrgang = jahrgang; // impliziert cast von int zu double
        System.out.println("Jahrgang als int= " + jahrgang);
        System.out.println("Jahrgang als double= " + dJahrgang);

        // Explizit
        double dPreis = 128.55d;
        // expliziter cast von double zu int, d.h. Datneverlust
        // weil der int keine Nachkommastellen hat
        int preis = (int) dPreis;
        System.out.println("Preis als double:" + dPreis); // 128.55
        System.out.println("Preis als int:" + preis); // 128

        int wert = 1;
        char buchstabe = (char) wert;
        System.out.println(buchstabe);
    }
}
