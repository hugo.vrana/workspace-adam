package hugo.io.azo.Wasserbehealter;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class WasserBehaelter {
    public Integer aktuelleVolume = 0;
    public Integer maxVolume = 0;
    public Boolean maxLimit = false;
    public Integer minVolume = 0;
    public Boolean minLimit = false;
    public Integer abfluss = 0;
    public Integer zufluss = 0;

    public void zufliessen(WasserBehaelter wb) {
        try {
            while (wb.maxLimit = false) {
                if ((wb.aktuelleVolume + wb.zufluss) >= wb.maxVolume) {
                    wb.aktuelleVolume = wb.maxVolume;
                    wb.maxLimit = true;
                } else {
                    wb.aktuelleVolume += wb.zufluss;
                }
            }
        } catch (Exception ex) {
            System.out.println("FEHLER beim zufliessen.");
        }
    }

    public void abfliessen(WasserBehaelter wb) {
        try {
            while (wb.minLimit = false) {
                if ((wb.aktuelleVolume - wb.abfluss) <= wb.minVolume) {
                    wb.aktuelleVolume = wb.minVolume;
                    wb.minLimit = true;
                } else {
                    wb.aktuelleVolume -= wb.abfluss;
                }
            }
        } catch (Exception ex) {
            System.out.println("FEHLER beim abfliessen.");
        }


    }

    public void start(WasserBehaelter wb) {
        try {
            zufliessen(wb);
            abfliessen(wb);
        } catch (Exception ex) {
            System.out.print("FEHLER beim start");
        }
    }

    public Integer eingabeWasser() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String eingabe = "";

        try {
            eingabe = reader.readLine();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

        Integer iEingabe = null;

        try {
            iEingabe = Integer.parseInt(eingabe);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

        return iEingabe;
    }

    public void wertDefinieren(WasserBehaelter wb) {
        // maxVolume
        System.out.println("Bitte geben Sie maximale Volume des Behaelters ein.");
        wb.maxVolume = wb.eingabeWasser();

        // MinVolume
        System.out.println("Bitte geben Sie minimale Volume des Behaelters ein.");
        wb.minVolume = wb.eingabeWasser();

        // Abfluss
        System.out.println("Bitte geben Sie den Abfluss des Behaelters ein.");
        wb.abfluss = wb.eingabeWasser();

        // Zufluss
        System.out.println("Bitte geben Sie den zufluss des Behaelters ein.");
        wb.zufluss = wb.eingabeWasser();
    }


    public void warten() {
        try {
            System.out.wait(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
