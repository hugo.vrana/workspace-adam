package hugo.io.azo.Ueb05bAufgD;

public class MainArrayKopieren {
    public static void main(String[] args) {
        String[] array1 = new String[5];
        array1[0] = "H";
        array1[1] = "A";
        array1[2] = "L";
        array1[3] = "L";
        array1[4] = "O";

        System.out.println("Erste Array");
        int a = 0;
        for (a = 0; a < 5; a++) {
            System.out.println(array1[a]);
        }

        String[] array2 = new String[5];
        array2[4] = array1[0];
        array2[3] = array1[1];
        array2[2] = array1[2];
        array2[1] = array1[3];
        array2[0] = array1[4];

        System.out.println("Zweite Array");
        for (a = 0; a < 5; a++) {
            System.out.println(array2[a]);
        }
    }
}