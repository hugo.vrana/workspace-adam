package hugo.io.azo.Ueb08aAufgD;

import java.util.ArrayList;
import java.util.Scanner;

import static java.lang.System.in;
import static java.lang.System.out;


public class MainParkhaus {
    public static void main(String[] args) {
        Scanner scn = new Scanner(in);
        ArrayList<Auto> parkhaus = new ArrayList<Auto>();

        while (true) {
            out.println("-------------------------------------------------------------------");
            out.println("P A R K H A U S");
            out.println("Anzahl Autos: " + parkhaus.size());
            out.println("1: Einfahrt");
            out.println("2: Ausfahrt");
            out.println("-------------------------------------------------------------------");

            switch (scn.nextInt()) {
                case 1: {
                    if (parkhaus.size() >= 5) {
                        out.println("Parkhaus ist voll.");
                        continue;
                    }

                    Integer autonummer = parkhaus.size() + 1;
                    parkhaus.add(new Auto());
                    System.out.println("Nr. " + autonummer + " ist nun im Parkhaus.");
                    break;
                }
                case 2: {
                    if (parkhaus.size() < 1) {
                        out.println("Parkhaus ist leer");
                        continue;
                    }

                    for (Integer i = 1; i <= parkhaus.size(); ++i) {
                        out.println("Nr. " + i);
                    }

                    out.println("Autonummer eingeben: ");
                    Integer autoNr = scn.nextInt();

                    try {
                        parkhaus.remove(autoNr - 1);
                    } catch (Exception ex) {
                        out.println("Autonummer nicht vorhanden.");
                        continue;
                    }
                    break;
                }
                default:
                    out.println("Eingabe nicht verstanden.");
                    break;
            }
            scn.close();
        }
    }
}