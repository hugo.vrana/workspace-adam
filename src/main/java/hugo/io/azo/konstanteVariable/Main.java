package hugo.io.azo.konstanteVariable;

/**
 * Created by hvran on 10.03.2017.
 */
public class Main {
    public static void main(String[] Args) {
        final int konstanzBla = 54; // mit final bestimmt man eine Konstanz, die dann nicht mehr geändert werden kann
        int variableBla = 55; // dieser Int kann man noch ändern
    }
}
