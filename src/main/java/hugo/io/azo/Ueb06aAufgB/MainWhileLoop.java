package hugo.io.azo.Ueb06aAufgB;

public class MainWhileLoop {

    public static void main(String[] args) {
        // forward
        int id = 97;
        while (id < 123) {
            char buchstabe = (char) id;
            System.out.print(buchstabe + " ");
            id++;
        }

        System.out.println("\n");

        // backward
        id = 122;
        while (id > 96 && id < 123) {
            char buchstabe = (char) id;
            System.out.print(buchstabe + " ");
            id--;
        }
    }
}