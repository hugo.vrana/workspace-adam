package hugo.io.azo.Ueb06aAufgC;

public class MainForEachLoop {
    public static void main(String[] args) {

        String[] monate =
                {
                        "Januar",
                        "Februar",
                        "März",
                        "April",
                        "Mai",
                        "Juni",
                        "Juli",
                        "August",
                        "September",
                        "Oktober",
                        "November",
                        "Dezember",
                };

        for (String monat : monate) {
            System.out.println(monat);
        }
    }
}