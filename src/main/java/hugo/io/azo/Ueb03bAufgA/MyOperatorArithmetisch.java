package hugo.io.azo.Ueb03bAufgA;

public class MyOperatorArithmetisch {
    public static void main(String[] args) {
        int lohn = 2000;
        int bonus = 500;

        int zahlung = lohn + bonus; //Addition
        System.out.println("Brutto Lohn : \t\t" + zahlung);

        double krankenKasse = zahlung / 100.0 * 11.5631; // Division und Multiplikation
        System.out.println("Krankenkasse Pr�mie: \t" + krankenKasse + "Fr.");

        int jahresEndeBonus = ++bonus; // ++ Operator
        System.out.println("JahresEndeBonus : \t" + jahresEndeBonus + "Fr.");

        int restDerDivision = bonus % 6; //Modulo - Operator %
        System.out.println("Rest der Division: \t" + restDerDivision + " Fr.");
    }
}
