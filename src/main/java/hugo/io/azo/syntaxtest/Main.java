package hugo.io.azo.syntaxtest;

import hugo.io.other.cosoleUtility.myconsole.MyConsole;

import java.util.ArrayList;
import java.util.List;

public class Main {

    private static MyConsole myConsole = new MyConsole();

    public static void main(String[] args) {

        List<String> menuItems = new ArrayList<>();
        menuItems.add("Is it Integer or Float?");
        menuItems.add("Is your credit card number valid ?");

        myConsole.printMenu(menuItems);

        Integer choosenValue = myConsole.readLineInt();

        switch (choosenValue) {
            case 1:
                floatOrInt();
                break;
            case 2:
                creditCardNumber();
                break;
            default:
                myConsole.printLine("You canno't choose this value. The program will quit.");
                break;
        }
    }

    /*<summary>
    Comtrolls if the entered value is number(float or int) or not.
    </summary>*/
    private static void floatOrInt() {
        System.out.println("Please insert any number.");
        String input = myConsole.readLineString();

        if (input.contains(".")) {

            Float number = Float.valueOf(input);
            myConsole.printLine(number + " is float");
        } else {
            try {
                Integer number = Integer.valueOf(input);
                myConsole.printLine(number + " is Integer");
            } catch (Exception ex) {
                myConsole.printLine("You did not inserted a proper number.");
            }
        }
    }

    /*<summary>
    Proofs the basic format of an credit card number.
    </summary>*/
    private static void creditCardNumber() {
        myConsole.printLine("Please instert the credit card number.");
        String creditCardNum = myConsole.readLineString();
        String regEx = "'\'d{7}";
        if (creditCardNum.matches(regEx)) {
            myConsole.printLine("Your number might be real credit card number.");
        } else {
            myConsole.printLine("Your number is not a real credit card number.");
        }
    }
}