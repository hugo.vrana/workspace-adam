package hugo.io.azo.Ueb05bAufgE;

public class MainArrayWrite {
    public static void main(String[] args) {
        String[] meineArray = new String[5];
        int i = 0;

        //schreiben
        for (i = 0; i < 5; i++) {
            meineArray[i] = "Hallo, Java";
        }

        // lesen
        for (i = 0; i < 5; i++) {
            System.out.println(meineArray[i]);
        }
    }
}