package hugo.io.azo.converterKmMi;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Define conversion (km to miles/miles to kilometers): km to miles");

        Integer input = input();
        switch (input) {
            case 1:
                kmToMi();
                break;
            case 2:
                miToKm();
                break;
            default:
                kmToMi();
                break;
        }
    }

    public static Double kmToMi() {
        System.out.println("Please give a number. (km)");
        System.out.println("Later it will be convertet in miles.");
        Integer input = input();

        // converting from String in Integer
        Integer iKm = 0;
        try {
            iKm = Integer.valueOf(input);
        } catch (Exception ex) {
            System.out.println("Error: Converting from String zo Integer. " + ex.getMessage());
        }

        Double dKm = (double) iKm; // wird auf xx.00 aufgerundet
        System.out.println("Your value: " + dKm + "Kilometers");
        try {
            // Converting from Km to Miles
            Double miFactor = 0.621371;
            Double mi = miFactor * dKm;
            System.out.println("New value: " + mi + " Miles");
            return mi;
        } catch (Exception ex) {
            System.out.println("Error: Converting from km - mi: " + ex.getMessage());
            return Double.MIN_VALUE;
        }
    }

    public static Double miToKm() {
        System.out.println("Please give a number. (mi)");
        System.out.println("Later it will be convertet in kilometers.");
        Integer input = input();
        // converting from String in Integer
        Integer iMi = 0;
        try {
            iMi = Integer.valueOf(input);
        } catch (Exception ex) {
            System.out.println("Error: Converting from String zo Integer. " + ex.getMessage());
        }

        Double dMi = (double) iMi; // wird auf xx.00 aufgerundet
        System.out.println("Your value: " + dMi + " Miles");

        // Converting from Miles to Km
        Double kmFactor = 1.609344;
        Double km = kmFactor * dMi;
        System.out.println("New value: " + km + " Kilometers");
        return km;
    }

    public static Integer input() {
        Integer input = null;
        try {
            Scanner sc = new Scanner(System.in);
            if (sc.hasNext()) {
                input = sc.nextInt();
            } else {
                System.out.println("Error: Scanner did not worked.");
                input = 0;
            }
        } catch (Exception ex) {
            System.out.println("Error: " + ex.getMessage());
            input = 0;
        } finally {
            return input;
        }

    }
}
