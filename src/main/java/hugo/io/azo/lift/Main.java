package hugo.io.azo.lift;

public class Main {
    public static void main(String[] Args) {

        Lift lift = new Lift();

        while (true) {
            if (!lift.amFahren) {
                lift.rufen();
                lift.stockWahlen();
                lift.stoppen();
                lift.turOeffnen();
            }
        }
    }
}