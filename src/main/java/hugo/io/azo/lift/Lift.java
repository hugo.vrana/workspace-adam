package hugo.io.azo.lift;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Lift {
    public static Integer minStock = 0;
    public static Integer maxStock = 10;
    public static Boolean turGeoffnet = false;
    public static Boolean amFahren = false;
    public static Boolean zielErreicht = false;
    public static Integer aktuellerStock = 0;
    public static Integer zielStock = 0;
    public static String turZustand = "";

    // Da werden die Tur geoffnet
    public static void turOeffnen() {
        // wenn ist Tur zugemacht
        if (!turGeoffnet) {
            System.out.println("Tur werden geoffnet");
            turGeoffnet = true;
            turSchliessen();
        } else {
            turSchliessen();
        }
    }

    public static void turSchliessen() {
        System.out.println("Tur werden zugemacht");
        turGeoffnet = false;
    }

    public static void rufen() {
        System.out.println("Lift wird gerufen");
        fahren();
    }

    public static void fahren() {
        amFahren = true;
        zielErreicht = true;
        aktuellerStock = zielStock.intValue();
        amFahren = false;
        turOeffnen();
    }

    public static void stockWahlen() {
        System.out.print("Bitte geben sie ein beliebiges Stockwerk (1-20)");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            Integer input = Integer.parseInt(reader.readLine());
            if (input < maxStock && input > minStock) {
                System.out.println("Sie werden nach " + input + " gefahren.");
                zielStock = input.intValue();
            } else {
                System.out.println("Das von ihnen gewähltes Stockwerk gibt es nicht");
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            zielStock = 0;
        }
    }

    public static Boolean stoppen() {
        Boolean amFahren = false;
        return amFahren;
    }

    public static void turZustand() {
        if (Lift.turGeoffnet = true) {
            Lift.turZustand = "geöffnet";
        } else {
            Lift.turZustand = "geschlossen";
        }
    }
}