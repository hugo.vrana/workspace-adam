package hugo.io.azo.Ueb08cAufgC;

import java.util.Comparator;

public class PlzComparer implements Comparator<Person> {
    private SortAlgo sort = SortAlgo.ASC;

    public PlzComparer() {

    }

    public PlzComparer(SortAlgo ascOrDesc) {
        this.sort = ascOrDesc;
    }

    @Override
    public int compare(Person a, Person b) {
        switch (sort) {
            case ASC: {
                if (a.getPlz().compareTo(b.getPlz()) > 0)
                    return 1;
                else if (a.getPlz().compareTo(b.getPlz()) < 0)
                    return -1;
                return 0;
            }

            case DESC: {
                if (a.getPlz().compareTo(b.getPlz()) < 0)
                    return 1;
                else if (a.getPlz().compareTo(b.getPlz()) > 0)
                    return -1;
                return 0;
            }
            default:
                break;
        }
        return 0;
    }

    public enum SortAlgo {ASC, DESC}

}
