package hugo.io.azo.Ueb08cAufgC;

public class Person {
    private String vorname;
    private String nachname;
    private Integer plz;
    private String city;

    public Person(String sVorname, String sNachname, Integer iPlz, String sCity) {
        vorname = sVorname;
        nachname = sNachname;
        plz = iPlz;
        city = sCity;
    }

    public String getVorname() {
        return vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public Integer getPlz() {
        return plz;
    }

    public String getCity() {
        return city;
    }
}
