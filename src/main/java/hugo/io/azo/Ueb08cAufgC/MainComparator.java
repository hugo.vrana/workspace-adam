package hugo.io.azo.Ueb08cAufgC;

import java.util.ArrayList;
import java.util.List;

public class MainComparator {
    public static void main(String[] args) {
        List<Person> personen = new ArrayList<Person>();
        try {
            personen.add(new Person("Peter", "Muster", 8400, "Winterthur"));
            personen.add(new Person("Susi", "Sorglos", 8399, "Hintertutigen"));
            personen.add(new Person("Hans", "Testfall", 8401, "Winterthur"));
            personen.add(new Person("Anne", "Frank", 9000, "St. Gallen"));
            personen.add(new Person("John", "Google", 8000, "Z�rich"));
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        System.out.println("aufsteigend weil default");
        personen.sort(new PlzComparer());

        for (Person person : personen) {
            System.out.println(person.getVorname() + " " + person.getNachname() + " " + person.getPlz() + " " + person.getCity());
        }
        System.out.println("");
        System.out.println("absteigend");
        personen.sort(new PlzComparer(PlzComparer.SortAlgo.DESC));

        for (Person person : personen) {
            System.out.println(person.getVorname() + " " + person.getNachname() + " " + person.getPlz() + " " + person.getCity());
        }
    }
}