package hugo.io.azo.Ueb08dAufgA;

import java.util.ArrayList;

public class MainForEachZahlen {
    public static void main(String[] args) {
        ArrayList<Integer> autoNummern = new ArrayList<Integer>();
        autoNummern.add(1);
        autoNummern.add(2);
        autoNummern.add(7);
        autoNummern.add(5);
        autoNummern.remove(0); // entfernt das erste Element in der Liste

        /*Die Syntax mit dem Pfeil-Symbol in hugo.io.azo.forEach "->" ist etwas ungewohnt,
         * sie heisst "Lamba-Syntax".*/
        autoNummern.forEach(bla -> System.out.println(bla));
    }
}