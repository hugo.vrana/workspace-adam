package hugo.io.azo.Ueb08aAufgC;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class MainArrayListWhile {

    public static void main(String[] args) {
        ArrayList<String> strings = new ArrayList<String>();
        Integer i = 0;

        // Schreiben
        while (i < 3) {
            System.out.println("Bitte drei beliebige Strings eingeben.");
            String eingabe = eingabe();
            strings.add(eingabe);
            i++;
        }

        // Ausgeben
        i = 0;
        System.out.println("");
        System.out.println("RESULTAT");
        while (i < 3) {
            String ausgabe = strings.get(i);
            System.out.println(ausgabe);
            i++;
        }
    }

    public static String eingabe() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String eingabe = "";

        try {
            eingabe = reader.readLine();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            eingabe = "";
        }
        return eingabe;
    }

}
