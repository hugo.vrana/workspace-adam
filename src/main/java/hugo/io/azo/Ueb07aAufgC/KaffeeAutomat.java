package hugo.io.azo.Ueb07aAufgC;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class KaffeeAutomat {
    public Double iKredit = 0.0d;
    public Float GETRAENKEKOSTEN = 0.9f;
    public Integer iZucker = 0;

    public Integer menu(KaffeeAutomat kaffi) {
        try {
            System.out.println("Bitte wahlen Sie etwas aus dem Menu");
            Integer input = eingabe();
            switch (input) {
                case 0:
                    geldEingeben(kaffi);
                    break;

                case 1:
                    schwarz(kaffi);
                    break;

                case 2:
                    espresso(kaffi);
                    break;

                case 3:
                    ovomaltine(kaffi);
                    break;

                case 9:
                    rueckgeld(kaffi);
                    break;
            }
            return 0;
        } catch (Exception ex) {
            System.out.println("Fehler in der Funktion eingabe.");
            return eingabe();
        }

    }

    public void zahlen(KaffeeAutomat kaffi) {
        kaffi.iKredit -= 0.9;
    }

    public void zucker(KaffeeAutomat kaffi) {
        System.out.println("Bitte Anzahl Zucker eingeben");
        kaffi.iZucker = eingabe();
        System.out.println("Sie haben " + iZucker + " Zucker gewaehlt.");
        kaffi.iZucker = 0;

    }

    public void schwarz(KaffeeAutomat kaffi) {
        zucker(kaffi);
        zahlen(kaffi);
        System.out.println("Ihre schwarzes Kaffee wird vorbereitet");
        System.out.println("Ihr schwarzes kafee ist fertig. Bitte, genuessen Sie unseres Produkt.");
    }

    public void espresso(KaffeeAutomat kaffi) {
        zucker(kaffi);
        zahlen(kaffi);
        System.out.println("Ihre schwarzes Kaffee wird vorbereitet");
        System.out.println("Ihr schwarzes kafee ist fertig. Bitte, genuessen Sie unseres Produkt.");
    }

    public void ovomaltine(KaffeeAutomat kaffi) {
        zucker(kaffi);
        zahlen(kaffi);
        System.out.println("Ihre schwarzes Kaffee wird vorbereitet");
        System.out.println("Ihr schwarzes kafee ist fertig. Bitte, genuessen Sie unseres Produkt.");
    }

    public void geldEingeben(KaffeeAutomat kaffi) {
        System.out.println("Geben sie Geld ein: ");
        try {
            kaffi.iKredit = kaffi.iKredit + eingabe().doubleValue();
        } catch (Exception ex) {
            System.out.println("FEHLER");
        }
    }

    public Integer eingabe() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String eingabe = "";

        try {
            eingabe = reader.readLine();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

        Integer iEingabe = null;

        try {
            iEingabe = Integer.parseInt(eingabe);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

        return iEingabe;
    }

    private void rueckgeld(KaffeeAutomat kaffi) {
        System.out.println("Es wurde " + kaffi.iKredit + " zur�ckgegeben.");
    }
}