package hugo.io.azo.textFileReader;

import hugo.io.other.cosoleUtility.myconsole.MyConsole;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.time.Year;

public class Main {
    public static void main(String[] argv) {
        MyConsole console = new MyConsole();
        // this file can be found in the res file
        String fileName = "namelist.txt";
        try {
            String inputLine;
            String filePath = "res/namelist.txt";
            FileReader fr = new FileReader(filePath);
            BufferedReader br = new BufferedReader(fr);
            Boolean found = Boolean.FALSE;
            console.printLine("Insert the persons name. If not inserted, all persons will be shown");
            String name = console.readLineString();
            while ((inputLine = br.readLine()) != null) {
                String[] lineParts = inputLine.split(";");
                for (String p : lineParts) {
                    if (p.contains(name)) {
                        if ((p.contains("1") || p.contains("2"))) {
                            Year bornYear = Year.parse(p);
                            Year nowYear = Year.now();
                            Integer age = Integer.valueOf(nowYear.toString()) - Integer.valueOf(bornYear.toString());
                            inputLine = inputLine + "; Age: " + age;
                        }
                        console.printLine(inputLine);

                        break;
                    }
                }
            }

            ///
            /// Read users input
            ///
            console.printLine("");
            console.printLine("Add new Person");
            console.printLine("Insert first name");
            String firstName = console.readLineString();
            console.printLine("Insert second name");
            String secondName = console.readLineString();
            console.printLine("Insert birth year");
            String newBirthYear = console.readLineString();
            Year birthYear = Year.now();
            try {
                birthYear = Year.parse(newBirthYear);
            } catch (Exception ex) {
                birthYear = Year.now();
            }

            ///
            /// Write in file
            ///
            PrintWriter pw = new PrintWriter(new FileWriter(filePath, true));
            String values = System.getProperty("line.separator") + firstName + ";" + secondName + ";" + birthYear;
            try {

                pw.append(values);
            } catch (Exception ex) {
                console.printLine("Error by writing in the file");
            }
            pw.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private static void search() {

    }
}