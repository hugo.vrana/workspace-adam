package hugo.io.azo.shapes;

import hugo.io.azo.shapes.model.Circle;
import hugo.io.azo.shapes.model.Rectangle;
import hugo.io.azo.shapes.model.Triangle;

public class Main {
    public static void main(String[] args) {
        GeoManager geoManager = new GeoManager();
        geoManager.addShape(new Triangle(Double.valueOf(2), Double.valueOf(5)));
        geoManager.addShape(new Circle(Double.valueOf(56)));
        geoManager.addShape(new Rectangle(Double.valueOf(345678), Double.valueOf(345678)));
        geoManager.print();
    }
}
