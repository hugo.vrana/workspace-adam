package hugo.io.azo.shapes.model;

public class Rectangle implements Shape {
    private Double height;
    private Double length;

    public Rectangle(Double height, Double length) {
        super();
        this.height = height;
        this.length = length;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    @Override
    public Double calculateArea() {
        return this.height * this.length;
    }
}
