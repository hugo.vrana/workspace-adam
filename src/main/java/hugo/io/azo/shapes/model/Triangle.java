package hugo.io.azo.shapes.model;

public class Triangle implements Shape {
    private Double basesurfance;
    private Double height;

    public Triangle(Double basesurfance, Double height) {
        super();
        this.basesurfance = basesurfance;
        this.height = height;
    }

    public Double getBasesurfance() {
        return basesurfance;
    }

    public Double getHeight() {
        return height;
    }

    @Override
    public Double calculateArea() {
        return (this.basesurfance * this.height) / 2;
    }
}
