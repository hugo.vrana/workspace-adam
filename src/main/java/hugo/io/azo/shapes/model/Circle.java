package hugo.io.azo.shapes.model;

public class Circle implements Shape {
    private Double radius;

    public Circle(Double radius) {
        this.radius = radius;
    }

    public Double getRadius() {
        return radius;
    }

    @Override
    public Double calculateArea() {
        return Math.PI * radius * 2;
    }
}
