package hugo.io.azo.shapes.model;

public interface Shape {
    Double calculateArea();
}
