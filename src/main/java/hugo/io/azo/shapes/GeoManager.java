package hugo.io.azo.shapes;

import hugo.io.azo.shapes.model.Circle;
import hugo.io.azo.shapes.model.Rectangle;
import hugo.io.azo.shapes.model.Shape;
import hugo.io.azo.shapes.model.Triangle;
import hugo.io.other.cosoleUtility.myconsole.MyConsole;

import java.util.ArrayList;

public class GeoManager {
    private ArrayList<Shape> shapes = new ArrayList<>();

    public void addShape(Shape shape) {
        this.shapes.add(shape);
    }

    public void print() {
        MyConsole console = new MyConsole();
        for (Shape s : shapes) {
            String text = "Shape:";
            // is triangle
            if (s.getClass().equals(Triangle.class)) {
                text += "triangle";
                Triangle t = (Triangle) s;
                text += ", Area= " + t.calculateArea();
                text += ", Height= " + t.getHeight();
                text += ", Base Surfance= " + t.getBasesurfance();
            } else if (s.getClass().equals(Circle.class)) {
                text += "circle";
                Circle circle = (Circle) s;
                text += ", Radius= " + circle.getRadius();
                text += ", Area= " + circle.calculateArea();
            } else if (s.getClass().equals(Rectangle.class)) {
                text += "rectangle";
                Rectangle rectangle = (Rectangle) s;
                text += ", Height= " + rectangle.getHeight();
                text += ", Length= " + rectangle.getLength();
                text += ", Area= " + rectangle.calculateArea();
            }
            console.printLine(text);
        }
    }
}
