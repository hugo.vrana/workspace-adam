package hugo.io.other.guessNumber;

import hugo.io.other.cosoleUtility.myconsole.MyConsole;

public class App {
    public Integer max;
    public Integer min;
    public Integer random;
    public Integer attempts = 0;

    public void main() {

        Boolean guessed = Boolean.FALSE;
        MyConsole console = new MyConsole();
        console.printLine("Please think of an number between 0 and 100");
        max = 100;
        min = 0;

        while (!guessed) {
            attempts++;
            boolean answered = false;
            while (!answered) {
                random = (max + min) / 2;
                console.printLine("Is this your number : " + random + "?  Y/N");
                Character answer = console.readLineString().charAt(0);

                if (answer.toString().toUpperCase().equals("Y")) {
                    answered = true;
                    guessed = true;
                    console.printLine("Great, we guessed the number on " + attempts + " Attempts");
                } else if (answer.toString().toUpperCase().equals("N")) {
                    console.printLine("Is this number bigger than your number ? Y/N");
                    Character bigger = console.readLineString().charAt(0);
                    if (bigger.toString().toUpperCase().equals("Y")) {
                        answered = true;
                        max = random;
                    } else if (bigger.toString().toUpperCase().equals("N")) {
                        answered = true;
                        min = random;
                    } else {
                        answered = false;
                    }
                }
            }
        }
    }
}
