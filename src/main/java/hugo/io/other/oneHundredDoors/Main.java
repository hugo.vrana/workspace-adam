package hugo.io.other.oneHundredDoors;

import java.util.HashMap;
import java.util.Map;

/// http://rosettacode.org/wiki/100_doors
public class Main {
    public static void main(String[] args) {
        HashMap<Integer, Boolean> doors = new HashMap<>();
        for (int i = 0; i < 100; i++){
            // add 100 closed door
            // Boolean value represents if the door is closed or not
            doors.put(i, false);
        }

        int iterationCounter = 0; // counts up to 3
        for (int i = 0; i < 100; i++) {
            iterationCounter++;
            if(iterationCounter == 1){
                // toggle every door in map
                for (Map.Entry opened : doors.entrySet()) {
                    opened.setValue(!(Boolean) opened.getValue());
                }
            } else if (iterationCounter == 2){
                // visit every second door and toggle it
                for (Map.Entry opened : doors.entrySet()) {
                    Integer key = (Integer) opened.getKey();
                    if(key % 2 == 0) {
                        opened.setValue(!(Boolean) opened.getValue());
                    }
                }
            } else if (iterationCounter == 3){
                // visit every second door and toggle it
                for (Map.Entry opened : doors.entrySet()) {
                    Integer key = (Integer) opened.getKey();
                    if(key % 3 == 0) {
                        opened.setValue(!(Boolean) opened.getValue());
                    }
                }
                // reset the counter
                iterationCounter = 0;
            }
        }

        for(Map.Entry<Integer, Boolean> entry : doors.entrySet()) {
            System.out.println("Doors n. " + entry.getKey() + " : " + (entry.getValue() ? "opened" : "closed"));
        }
    }
}
