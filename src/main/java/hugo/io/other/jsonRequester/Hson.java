package hugo.io.other.jsonRequester;

import hugo.io.tbz.M411.webserviceClientPostDatabase.Post;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

///
/// Hugo Script Object Notation
///
public interface Hson<T> {

    // Converts the JSON String and saves the values in a hashmap
    //<param type = String name = json>JSON that should be parsed to object</param>
    // returns the converted object with values
    default T fromString(String json) {
        try {
            String s = json.replace("{", "").trim().replace("}", "").trim();
            String[] sa = s.split(",");
            Map<String, Object> map = new HashMap();
            for (String attribute : sa) {
                String[] aa = attribute.trim().split(":");
                String attributeName = aa[0].replace("\"", "").trim();
                if (aa[1].contains("\"")) {
                    if (aa[1].replace("\"", "").trim().contains("\n \n]")) {
                        map.put(attributeName, aa[1].replace("\"", "").trim().replace("\n \n]", ""));
                    } else {
                        map.put(attributeName, aa[1].replace("\"", "").trim());
                    }
                } else if (attributeName.contains("[\n  \n")) {
                    // comes in as "["userId": 1"
                    attributeName = attribute.replace(System.getProperty("line.separator"), "");
                    attributeName = attributeName.replace('[', Character.valueOf(' '));
                    attributeName = attributeName.replace("\"", "").trim();
                    attributeName = attributeName.replace(": 1", "").trim();
                    map.put(attributeName, 1);
                } else {
                    map.put(attributeName, Integer.valueOf(aa[1].trim()));
                }
            }
            T object = fromMap(map);
            return object;
        } catch (Exception ex) {
            return null;
        }
    }

    /// creates object from attributes that are saved in a Map with the key and a value
    //<param type = Map<String, Object> name = map> Map with values that should be convertet to specific object</param>
    // returns the converted object with values
    default T fromMap(Map<String, Object> map) {
        Field[] fields = this.getClass().getDeclaredFields();
        for (Field field : fields) {
            String attributeName = field.getName();
            // Reflection
            if (map.get(attributeName) instanceof Integer) {
                this.set(attributeName, (Integer) map.get(attributeName));
            } else {
                this.set(attributeName, (String) map.get(attributeName));
            }
        }
        return (T) this;
    }

    // setter for any Attribute in object
    //<param type = String name = attributeName> Speccific Atttribite name, in witch the value should be saved</param>
    // <param type = Integer name = value> The Integer value that should be written in the attribute</param>
    default void set(String attributeName, Integer value) {
        try {
            Method method = this.getClass().getDeclaredMethod("set" + capitalise(attributeName), Integer.class);
            method.invoke(this, value);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    // setter for any Attribute in object
    //<param type = String name = attributeName> Speccific Atttribite name, in witch the value should be saved</param>
    // <param type = String name = value> The String value that should be written in the attribute</param>
    default void set(String attributeName, String value) {
        try {
            Method method = this.getClass().getDeclaredMethod("set" + capitalise(attributeName), String.class);
            method.invoke(this, value);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    // Sets the first letter to upper case
    //<param type = String name = s>String in witch the first letter should be in capital</param>
    // returns the string with first letter in uppercase
    default String capitalise(String s) {
        return Character.toString(s.charAt(0)).toUpperCase() + s.substring(1);
    }

}
