package hugo.io.other.Infinity;

import hugo.io.other.cosoleUtility.myconsole.MyConsole;

public class Main {
    public static void main(String[] args) {
        double inf = Double.POSITIVE_INFINITY;
        double notInf = inf - 1;
        MyConsole console = new MyConsole();
        console.printLine(notInf);
    }
}
