package hugo.io.other.iec;

import hugo.io.other.jsonRequester.JsonRequester;
import java.util.ArrayList;

/// Searches the word list from http://wiki.puzzlers.org/pub/wordlists/unixdict.txt
///
public class Main {
    public static void main(String[] args) {
        JsonRequester jsonRequester = new JsonRequester();
        String json = jsonRequester.getBody("http://wiki.puzzlers.org/pub/wordlists/unixdict.txt");
        String[] words = json.split("\n");
        ArrayList<String> validWords = new ArrayList<>();
        for (String word : words) {
            if(word.contains("c")) {
                Integer firstC = word.indexOf('c');
                Integer firstIE = word.indexOf("ie");
                Integer firstEI = word.indexOf("ei");
                if(firstIE > firstC || firstEI > firstC){
                    validWords.add(word);
                }
            }
        }
        // print valid words
        for (String validWord : validWords) {
            System.out.println(validWord);
        }
    }
}
