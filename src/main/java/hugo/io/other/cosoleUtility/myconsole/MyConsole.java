package hugo.io.other.cosoleUtility.myconsole;

import java.util.List;
import java.util.Scanner;

public class MyConsole {

    public String readLineString() {
        String input = null;
        Scanner scanner = new Scanner(System.in);
        input = scanner.nextLine();
        return input;
    }

    public Integer readLineInt() {
        try {
            String toConvert = readLineString();
            Integer converted = Integer.valueOf(toConvert);
            return converted;
        } catch (Exception e) {
            printLine("Your value was not a number");
            return 0;
        }
    }

    public Float readLineFloat() {
        try {
            String toConvert = readLineString();
            Float converted = Float.valueOf(toConvert);
            return converted;
        } catch (Exception e) {
            printLine("Your value was not a number");
            return Float.valueOf(0);
        }
    }

    public Double readLineDouble() {
        try {
            String toConvert = readLineString();
            Double converted = Double.valueOf(toConvert);
            return converted;
        } catch (Exception e) {
            printLine("Your value was not a number");
            return Double.valueOf(0);
        }
    }

    public void printLine() {
        System.out.println();
    }

    public void printLine(String str) {
        System.out.println(str);
    }

    public void printLine(Integer i) {
        System.out.println(i);
    }

    public void printLine(Boolean b) {
        System.out.println(b);
    }

    public void printLine(Double d) {
        System.out.println(d);
    }

    public void printLine(Float f) {
        System.out.println(f);
    }

    public void printLine(Long l) {
        System.out.println(l);
    }

    public void printLine(byte b) {
        System.out.println(b);
    }

    public void printLine(Character c) {
        System.out.println(c);
    }

    public void print() {
        System.out.print("");
    }

    public void print(String str) {
        System.out.print(str);
    }

    public void print(Integer i) {
        System.out.print(i);
    }

    public void print(Double d) {
        System.out.print(d);
    }

    public void print(Float f) {
        System.out.print(f);
    }

    public void print(Long l) {
        System.out.print(l);
    }

    public void print(byte b) {
        System.out.print(b);
    }

    public void print(Character c) {
        System.out.print(c);
    }

    public void printMenu(List<String> menuItems) {
        for (String s : menuItems) {
            int index = menuItems.indexOf(s) + 1;
            System.out.println(index + ".) " + s);
        }
        System.out.println("What do you want to do?");
    }

    public void printList(List<String> list) {
        if (!list.isEmpty()) {
            for (String s : list) {
                printLine(s);
            }
        } else {
            System.out.println("Your list is empty.");
        }
    }

    public void printNumberedList(List<String> list) {
        if (!list.isEmpty()) {
            for (int i = 0; i < list.size(); i++) {
                int y = i;
                y++;
                printLine(y + ".) " + list.get(i).toString());
            }
        } else {
            printLine("Your list is empty.");
        }
    }
}
