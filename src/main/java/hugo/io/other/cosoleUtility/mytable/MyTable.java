package hugo.io.other.cosoleUtility.mytable;

import hugo.io.other.cosoleUtility.myconsole.MyConsole;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MyTable {

    private Integer collumCount = 0;

    public Integer getCollumCount() {
        return collumCount;
    }

    public void setColumCount(Integer collumCount) {
        this.collumCount = collumCount;
    }

    public void printTableCell(String value, Boolean bottomCell){
        MyConsole console = new MyConsole();
        Integer valueLength = value.length();
        String border = "--";
        for (int i = 0; i < valueLength; i++) {
            border += '-';
        }
        console.printLine(border);
        console.print(value);
        console.printLine('|');
        if(bottomCell){
            console.printLine(border);
        }
    }

    public void printArrayListInOneRow(ArrayList<?> values){
        MyConsole myConsole = new MyConsole();
        setColumCount(values.size());
        Integer lengthOfTopBorder = values.size();
        for (Object value: values) {
            lengthOfTopBorder += value.toString().length();
        }

        // Print top Border
        String horizontalBorder = "";
        for (int i = 0; i < lengthOfTopBorder; i++) {
            horizontalBorder += "-";
        }
        myConsole.printLine(horizontalBorder);
        myConsole.print('|');
        for (Object value: values) {

            myConsole.print(value.toString() + '|');
        }
        myConsole.printLine();
        myConsole.printLine(horizontalBorder);
    }

    public void print(HashMap<?, ?> table){
        MyConsole myConsole = new MyConsole();
        Integer maxRowLength = 0;
        for (Map.Entry<?, ?> row : table.entrySet()) {
            int rowLengt = row.getKey().toString().length() + row.getValue().toString().length();
            rowLengt += 8; // | [Value] | [Value] |
            if(rowLengt > maxRowLength){
                maxRowLength = rowLengt;
            }
        }
        String horizontlBorder = "";
        for(int i = 0; i < maxRowLength; i++){
            horizontlBorder += '-';
        }
        myConsole.printLine(horizontlBorder);
        for (Map.Entry<?, ?> row : table.entrySet()) {
            myConsole.printLine('|' + row.getKey().toString() + '|' + row.getValue().toString() + '|');
        }

    }
}
