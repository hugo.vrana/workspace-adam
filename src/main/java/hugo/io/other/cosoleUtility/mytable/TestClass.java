package hugo.io.other.cosoleUtility.mytable;

import java.util.ArrayList;
import java.util.HashMap;

public class TestClass {
    public static void main(String[] args) {
        MyTable myTable = new MyTable();

        ArrayList<String> stringArrayList = new ArrayList<>();
        stringArrayList.add("c");
        stringArrayList.add("cc");
        stringArrayList.add("");
        stringArrayList.add("You have the correct idea, but wrong syntax. In Java, only arrays support the [] syntax. An ArrayList isn't an array, it's a class that implements the List interface, and you should use the get method to access its members. Similarly, a String doesn't have a size() method, it has a length() method.");
        myTable.printArrayListInOneRow(stringArrayList);


        HashMap<Integer, String> hashMap = new HashMap<>();
        hashMap.put(1, "one");
        hashMap.put(2, "two");
        hashMap.put(3, "three");
        hashMap.put(4, "elevenrtzuiop");
        myTable.print(hashMap);

    }
}
