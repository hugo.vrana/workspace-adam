package hugo.io.tbz.M326.party;

import org.junit.Test;
import java.time.LocalDateTime;

public class MainTest {

    /**
     * Contructor test Person host = new Host(); -> Host inherited from Person
     * In repository Host as well as person can be added, since repository accepts persons as argument
     * */
    @Test
    public void InheritanceTest(){
        Repository repository = new Repository();
        Person host = new Host();
        repository.add(host);
        Person guest = new Person();
        repository.add(guest);
    }

    /**
     * Tests if some Objects can be created and added to repository
     */
    @Test
    public void Create() {
        Repository repository = new Repository();
        Location partyLocation = new Location(repository.getNewId(), "Partystreet", 23, "New York", 304);
        repository.add(partyLocation);

        Party party = new Party(repository.getNewId(), partyLocation, LocalDateTime.of(2019, 12, 5, 14, 30));
        repository.add(party);

        Location guests1Location = new Location(repository.getNewId(), "Masterstreet", 15, "London", 7892);
        repository.add(guests1Location);

        Location guest2Location = new Location(repository.getNewId(), "Imaginarystreet", 32, "Zurich", 123);

        Person guest1 = new Person(repository.getNewId(), "Mr", "Peter", "Muster", LocalDateTime.now(), guests1Location);
        repository.add(guest1);

        Person guest2 = new Person(repository.getNewId(), "Mrs", "Viviana", "Müller", LocalDateTime.now(), guest2Location);
        repository.add(guest2);

        Invitation invitationForGuest1 = new Invitation(repository.getNewId(), guest1, party);
        repository.add(invitationForGuest1);

        Invitation invitationForGuest2 = new Invitation(repository.getNewId(), guest2, party);
        invitationForGuest2.setAccepted(true);
        repository.add(invitationForGuest2);
    }
}
