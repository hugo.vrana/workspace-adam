package hugo.io.tbz.M226a.sportschef;

import junit.framework.TestCase;
import org.junit.Test;
import java.util.ArrayList;
import static junit.framework.TestCase.assertEquals;

public class MainTest {

    @Test
    public void test01_getSetNameSportschef() throws Exception {
        Team team = Team.getInstance();

        Sportschef sportschef = Sportschef.getInstance();
        sportschef.setName("Sportschef");

        team.setSportschef(sportschef);

        assertEquals("Sportschef", team.getSportschef().getName());
    }

    @Test
    public void test02_getSetNameTrainer() throws Exception {
        Team team = Team.getInstance();

        Trainer trainer = Trainer.getInstance();
        trainer.setName("Trainer");

        Sportschef sportschef = Sportschef.getInstance();
        sportschef.setName("Sportschef");

        sportschef.setTrainer(trainer);
        team.setSportschef(sportschef);

        assertEquals("Trainer", team.getSportschef().getTrainer().getName());
    }

    @Test
    public void test03_getSetNameGoalie() throws Exception {
        Team team = Team.getInstance();

        Sportschef sportschef = Sportschef.getInstance();
        sportschef.setName("Sportschef");

        Trainer trainer = Trainer.getInstance();
        trainer.setName("Trainer");

        Goalie goalie = Goalie.getInstance();
        goalie.setName("Goalie");

        trainer.setGoalie(goalie);
        sportschef.setTrainer(trainer);
        team.setSportschef(sportschef);

        assertEquals("Goalie", team.getSportschef().getTrainer().getGoalie().getName());
    }

    @Test
    public void test04_getSetNameFeldspieler1() throws Exception {
        Team team = Team.getInstance();

        Sportschef sportschef = Sportschef.getInstance();
        sportschef.setName("Sportschef");

        Trainer trainer = Trainer.getInstance();
        trainer.setName("Trainer");

        Feldspieler feldspieler1 = Feldspieler.getInstance();
        feldspieler1.setName("Feldspieler1");

        ArrayList<Feldspieler> feldspielers = new ArrayList<>();
        feldspielers.add(feldspieler1);

        trainer.setFeldspielers(feldspielers);
        sportschef.setTrainer(trainer);
        team.setSportschef(sportschef);

        assertEquals("Feldspieler1", team.getSportschef().getTrainer().getFeldspielers().get(0).getName());
    }

    @Test
    public void test05_getSetNameFeldspieler2() throws Exception {
        Team team = Team.getInstance();

        Sportschef sportschef = Sportschef.getInstance();
        sportschef.setName("Sportschef");

        Trainer trainer = Trainer.getInstance();
        trainer.setName("Trainer");

        Feldspieler feldspieler1 = Feldspieler.getInstance();
        feldspieler1.setName("Feldspieler1");

        Feldspieler feldspieler2 = Feldspieler.getInstance();
        feldspieler2.setName("Feldspieler2");

        ArrayList<Feldspieler> feldspielers = new ArrayList<>();
        feldspielers.add(feldspieler1);
        feldspielers.add(feldspieler2);

        trainer.setFeldspielers(feldspielers);
        sportschef.setTrainer(trainer);
        team.setSportschef(sportschef);

        assertEquals("Feldspieler2", team.getSportschef().getTrainer().getFeldspielers().get(1).getName());
    }

    @Test
    public void test06_getSetNameFeldspieler3() throws Exception {
        Team team = Team.getInstance();

        Sportschef sportschef = Sportschef.getInstance();
        sportschef.setName("Sportschef");

        Trainer trainer = Trainer.getInstance();
        trainer.setName("Trainer");

        Feldspieler feldspieler1 = Feldspieler.getInstance();
        feldspieler1.setName("Feldspieler1");

        Feldspieler feldspieler2 = Feldspieler.getInstance();
        feldspieler2.setName("Feldspieler2");

        Feldspieler feldspieler3 = Feldspieler.getInstance();
        feldspieler3.setName("Feldspieler3");

        ArrayList<Feldspieler> feldspielers = new ArrayList<>();
        feldspielers.add(feldspieler1);
        feldspielers.add(feldspieler2);
        feldspielers.add(feldspieler3);

        trainer.setFeldspielers(feldspielers);
        sportschef.setTrainer(trainer);
        team.setSportschef(sportschef);

        assertEquals("Feldspieler3", team.getSportschef().getTrainer().getFeldspielers().get(2).getName());
    }

    @Test
    public void test06_getSetNameFeldspieler4() throws Exception {
        Team team = Team.getInstance();

        Sportschef sportschef = Sportschef.getInstance();
        sportschef.setName("Sportschef");

        Trainer trainer = Trainer.getInstance();
        trainer.setName("Trainer");

        Feldspieler feldspieler1 = Feldspieler.getInstance();
        feldspieler1.setName("Feldspieler1");

        Feldspieler feldspieler2 = Feldspieler.getInstance();
        feldspieler2.setName("Feldspieler2");

        Feldspieler feldspieler3 = Feldspieler.getInstance();
        feldspieler3.setName("Feldspieler3");

        Feldspieler feldspieler4 = Feldspieler.getInstance();
        feldspieler4.setName("Feldspieler4");

        ArrayList<Feldspieler> feldspielers = new ArrayList<>();
        feldspielers.add(feldspieler1);
        feldspielers.add(feldspieler2);
        feldspielers.add(feldspieler3);
        feldspielers.add(feldspieler4);

        trainer.setFeldspielers(feldspielers);
        sportschef.setTrainer(trainer);
        team.setSportschef(sportschef);

        assertEquals("Feldspieler4", team.getSportschef().getTrainer().getFeldspielers().get(3).getName());
    }

    @Test
    public void test07_getSetNameFeldspieler5() throws Exception {
        Team team = Team.getInstance();
        team.getSportschef().getTrainer().getFeldspielers().get(4).setName("Feldspieler5");
        String name = team.getSportschef().getTrainer().getFeldspielers().get(4).getName();
        assertEquals("Feldspieler5", name);
    }

    @Test
    public void test08_getSetNameFeldspieler6() throws Exception {
        Team team = Team.getInstance();
        team.getSportschef().getTrainer().getFeldspielers().get(5).setName("Feldspieler6");
        String name = team.getSportschef().getTrainer().getFeldspielers().get(5).getName();
        assertEquals("Feldspieler6", name);
    }

    @Test
    public void test09_getSetNameFeldspieler7() throws Exception {
        Team team = Team.getInstance();
        team.getSportschef().getTrainer().getFeldspielers().get(6).setName("Feldspieler7");
        String name = team.getSportschef().getTrainer().getFeldspielers().get(6).getName();
        assertEquals("Feldspieler7", name);
    }

    @Test
    public void test10_getSetNameFeldspieler8() throws Exception {
        Team team = Team.getInstance();
        team.getSportschef().getTrainer().getFeldspielers().get(7).setName("Feldspieler8");
        String name = team.getSportschef().getTrainer().getFeldspielers().get(7).getName();
        assertEquals("Feldspieler8", name);
    }

    @Test
    public void test11_getSetNameFeldspieler9() throws Exception {
        Team team = Team.getInstance();
        team.getSportschef().getTrainer().getFeldspielers().get(8).setName("Feldspieler9");
        String name = team.getSportschef().getTrainer().getFeldspielers().get(8).getName();
        assertEquals("Feldspieler9", name);
    }

    @Test
    public void test12_getSetNameFeldspieler10() throws Exception {
        Team team = Team.getInstance();
        team.getSportschef().getTrainer().getFeldspielers().get(9).setName("Feldspieler10");
        String name = team.getSportschef().getTrainer().getFeldspielers().get(9).getName();
        assertEquals("Feldspieler10", name);
    }

    @Test
    public void test13_getSetNameErsatzbank1() throws Exception {
        Team team = Team.getInstance();
        team.getSportschef().getTrainer().getErsatzspielers().get(0).setName("Ersatzbank1");
        String name = team.getSportschef().getTrainer().getErsatzspielers().get(0).getName();
        assertEquals("Ersatzbank1", name);
    }

    @Test
    public void test14_getSetNameErsatzbank2() throws Exception {
        Team team = Team.getInstance();
        team.getSportschef().getTrainer().getErsatzspielers().get(1).setName("Ersatzbank2");
        String name = team.getSportschef().getTrainer().getErsatzspielers().get(1).getName();
        assertEquals("Ersatzbank2", name);
    }

    @Test
    public void test15_getSetNameErsatzbank3() throws Exception {
        Team team = Team.getInstance();
        team.getSportschef().getTrainer().getErsatzspielers().get(2).setName("Ersatzbank3");
        String name = team.getSportschef().getTrainer().getErsatzspielers().get(2).getName();
        assertEquals("Ersatzbank3", name);
    }

    ///creates 2 teams and compares those
    ///return true if those are equal
    /// return false if those are not equal
    @Test
    public void test16_multipleTeamsPossible() throws Exception {
        Team menchesterUnited = Team.getInstance();
        Team barcelona = Team.getInstance();
        Team realMadrid = Team.getInstance();
        Team fcZurich = Team.getInstance();
        Team schwiftyTeam = Team.getInstance();

        //Menchester united
        TestCase.assertNotSame(menchesterUnited, barcelona);
        TestCase.assertNotSame(menchesterUnited, realMadrid);
        TestCase.assertNotSame(menchesterUnited, fcZurich);
        TestCase.assertNotSame(menchesterUnited, schwiftyTeam);
        TestCase.assertSame(menchesterUnited, menchesterUnited);

        // Barcelona
        TestCase.assertNotSame(barcelona, realMadrid);
        TestCase.assertNotSame(barcelona, fcZurich);
        TestCase.assertNotSame(barcelona, schwiftyTeam);
        TestCase.assertSame(barcelona, barcelona);

        // Real Madrid
        TestCase.assertNotSame(realMadrid, fcZurich);
        TestCase.assertNotSame(realMadrid, schwiftyTeam);
        TestCase.assertSame(realMadrid, realMadrid);

        // fcZurich
        TestCase.assertNotSame(realMadrid, schwiftyTeam);
        TestCase.assertSame(realMadrid, realMadrid);

        // Schwifty Team
        TestCase.assertSame(schwiftyTeam, schwiftyTeam);
    }
}